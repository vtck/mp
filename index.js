
var express = require('express')
var app = express()
var path = require("path")

app.use(express.static('public'))

app.get('/',function(req,res){
  res.sendFile(path.join(__dirname + '/public/client.html'))
})

app.listen(3001, function () {
  console.log('Example app listening on port 3000!')
})

require('./public/scripts/helpers/MatrixExtension')

var io = require('socket.io')(process.env.PORT || 3000)
var shortid = require('shortid')
var env = require('dotenv').config()
var fs = require('fs')

var Room = require('./public/scripts/modules/Room')
var Player = require('./public/scripts/modules/Player')
var BaseMob = require('./public/scripts/modules/BaseMob')
var globals = require('./public/scripts/modules/Globals')
var Globals = new globals()
var aabb = require('aabb-3d')

console.log(' ╭────────────────────╮')
console.log(' │   Server started   │')
console.log(' ╰────────────────────╯')

// var tickInterval = 100;
var tickInterval = 1000 / 90;
var currentTick = 0;
var lastTickTime = new Date;
var delta = 0;

// var sendTickInterval = 500;
var sendTickInterval = 1000 / 60;

var roomX = 100
var roomY = 100
var roomZ = 100

var room = new Room('Test', roomX, roomY, roomZ)
var roomEntities = []
var roomPlayers = []

// ************ ГЛАВНЫЙ ЦИКЛ СЕРВЕРА ************
var mainLoop = setInterval(function () {
    ++currentTick
    delta = 1000 / (new Date - lastTickTime)
    lastTickTime = new Date
    if (process.env.VERBOSE_TICKS == 1 ) {
        console.log('Main loop tick ' + currentTick + ' delta ' + delta +'ms')
    }
    for (pIndex in roomPlayers) {
        if (roomPlayers.hasOwnProperty(pIndex)) {
            var player = roomPlayers[pIndex]
            player.update(delta)
        }
    }
}, tickInterval)

// ************ / ГЛАВНЫЙ ЦИКЛ СЕРВЕРА ************

// ************ ГЛАВНЫЙ ЦИКЛ ОТПРАВКИ ТИКОВ ************
var sendTickIntervalId = setInterval(function () {

    let tickRoomPlayers = Object.keys(roomPlayers).map(function (k,i) {
        let item = roomPlayers[k]
        let ret = {
            'id': item.id,
            'xlen': item.xlen,
            'ylen': item.ylen,
            'zlen': item.zlen,
            'speed': item.speed,
            'rotation': {
                'x': item.rotation.x,
                'y': item.rotation.y,
                'z': item.rotation.z
            },
            'pitch': item.pitch,
            'position': {
                'x': item.position.x,
                'y': item.position.y,
                'z': item.position.z
            },
            'velocity': {
                'x': item.velocity.x,
                'y': item.velocity.y,
                'z': item.velocity.z
            }
        }
        return ret
    })

    for (pIndex in roomPlayers) {
        if (roomPlayers.hasOwnProperty(pIndex)){
            if (process.env.VERBOSE_TICKS == 1 ) console.log('emiting tick to' + pIndex);
            var player = roomPlayers[pIndex];

            player.socket.emit('serverTick', {
                "currentTick" : currentTick,
                "delta" : delta,
                "roomPlayers" : tickRoomPlayers
            })

        }
    }


  
}, sendTickInterval)


// ************ ГЛАВНЫЙ ЦИКЛ ОТПРАВКИ ТИКОВ ************


io.on('connection', function(socket){


    let $id = shortid.generate();
    var player = new Player(room.getWorld().getMatrix(), $id, "Player " + $id, socket);
    roomPlayers[$id] = player;

    console.log('Client connected, id:' , $id);

    socket.emit('loggedIn', {
        'player_id' : $id
    });

    socket.emit('loadMapMatrix', {
        'matrix'    : room.getWorld().getMatrix(),
        'lightmap'  : room.getWorld().getLightmap()
    });
    
    socket.on('disconnect', function() {
        console.log('Client disconnected. Total online: ' + Object.keys(roomPlayers).length);
        delete roomPlayers[$id];
        socket.broadcast.emit('disconnected', {id: $id});
    });

    socket.on("clientSendState", function(data) {
        if(roomPlayers[data.id]){
            roomPlayers[data.id].velocity = data.velocity;
            roomPlayers[data.id].rotation = data.rotation;
            roomPlayers[data.id].pitch = data.pitch;
        }
    })

    socket.on("clientActivateObject", function(data) {
        console.log(data);
        var activateCell = room.getWorld().getBlock(data.obj.x + 1, data.obj.y, data.obj.z);
        if (activateCell == 60) {
            // Сделать проверку на возможность открывать эту дверь
            io.sockets.emit("updateMatrix", {
                x: data.obj.x + 1,
                y: data.obj.y,
                z: data.obj.z
            })
        }
    });


})
