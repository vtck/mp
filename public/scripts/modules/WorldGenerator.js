var globals = require('./Globals');
var Globals = new globals();

var MatrixExtensions = require('./../helpers/MatrixExtension');
var floodFill = require("n-dimensional-flood-fill");

var square8x8 = require("./buildings/square8x8/Square8x8");


module.exports = class WorldGenerator {
    constructor(width, height, elevation, noise, rand, objectOptions) {

	    var start_time = new Date();

	    var objectsMatrix = function(x, y ,z){

	    	var layers = new Array(y);
	        for(var layer = 0; layer < y; layer++){
    	        var arr = [];
    	        for(var i=0; i < x; i++){
    	            arr.push([]);
    	            arr[i].push( new Array(z));
    	            for(var j=0; j < z; j++){
    	              arr[i][j] = 0;
    	            }
    	        }
	            layers[layer] = arr;
    	    }
	        return layers;
	    }(width, elevation, height);

	    function getBlock(x, y, z) {
	    	if (y in objectsMatrix)
	    		if (x in objectsMatrix[y])
	    			if (z in objectsMatrix[y][x])
	    				return objectsMatrix[y][x][z]
	    	return -1;
	    }

	    function setBlock(x, y, z, value) {
	    	if (y in objectsMatrix)
	    		if (x in objectsMatrix[y])
	    			if (z in objectsMatrix[y][x])
	    				objectsMatrix[y][x][z] = value;
	    }

	    var factorMin = objectOptions.factorMin;
	    var factorMax = objectOptions.factorMax;

	    // Генерация первого этажа и поверхности
	    var generatedObjects = 0;
	    var generatedRoomsData = [];
	    var floorLayer = 0;

	    // var building2 = new square8x8();
	    // var matrixBuilding = building2.getBaseMatrix();

	    // var lastHeight = 0;

	    // for (var layer = 0; layer < matrixBuilding.length; layer++) {
	    //     for (var rowIndex = 0; rowIndex < matrixBuilding[layer].length; rowIndex++) {
	    //     	for (var cellIndex = 0; cellIndex < matrixBuilding[layer][rowIndex].length; cellIndex++) {
	    //     		var value = matrixBuilding[layer][rowIndex][cellIndex];
     //    			objectsMatrix[layer+1][rowIndex][cellIndex+24] = value
	    //     	}
	    //     }
	    // }
	    // lastHeight += matrixBuilding.length;

	    
	    // if (false)
	    // for (var row in noise[floorLayer]) {

	    //     for (var cell in noise[floorLayer][row]) {

	    //         var value = noise[floorLayer][row][cell];

	    //         if (value >= factorMin && value <= factorMax) {

	    //         	// Проверить что первые координаты предполагаемого этажа не исключены как уже использованные на этом слое
	    //         	if (objectsMatrix[floorLayer][row][cell] !== 0) {
	    //         	    continue;  
	    //         	} 

	    //         	// Ширина и высота дома относительно фактора
	    //         	var range = factorMax - factorMin;
	    //         	var valueInRange = value - factorMin;
	    //         	var percentInRange = ( valueInRange / range ) * 100;

	    //             // Точки, которые будут включены в сначала только текущий участок
	    //             // После подтверждения что участок имеет внешнее пространство - координаты совмещаются с глобальными исключениями
	    //             var excluded_in_this_plane = [];

	    //             // По-умолчанию считаем, что участок имеет внешнее пространство из таких же точек
	    //             var hasOuterSpace = true;

	    //             // Определить район точки
	    //             var districtId = Globals.DISTRICTS.DEFAULT;
	    //             var currentIndoorCell = 0;
	    //             var row_padding = 4;
					// var cell_padding = 4;

	    //             if (percentInRange > 65) {

	    //             	districtId = Globals.DISTRICTS.UPTOWN.ID;
	    //                 currentIndoorCell = Globals.TILES.INDOOR.UPTOWN.ID;
	    //                 row_padding = rand.intBetween(4, 10);
	    //                 cell_padding = rand.intBetween(4, 10);

	    //             } else if (percentInRange > 40 ) {

	    //             	districtId = Globals.DISTRICTS.MIDTOWN.ID;
					// 	currentIndoorCell = Globals.TILES.INDOOR.MIDTOWN.ID;						
					// 	row_padding = rand.intBetween(4, 6);
	    //                 cell_padding = rand.intBetween(4, 6);

	    //             } else {

	    //             	districtId = Globals.DISTRICTS.SUBURB.ID;
	    //             	currentIndoorCell = Globals.TILES.INDOOR.SUBURB.ID;
	    //             	row_padding = rand.intBetween(2, 4);
	    //                 cell_padding = rand.intBetween(2, 4);
	    //             }


	    //             // Вытащить центральную точку в текущий план
	    //             excluded_in_this_plane.push({
	    //                 row: row, 
	    //                 cell: cell, 
	    //                 value: currentIndoorCell
	    //             });
	                
	               

	    //             // Рисуем внутреннее пространтво дома

	    //             for (var pr = -row_padding; pr <= row_padding; pr++) {

	    //             	var checkRow = parseInt(row) + pr;

	    //             	// Если текущей строки нет в шуме (верхние и нижние края карты), то считать что внешнего пространства нет
	    //             	if(!noise[floorLayer][checkRow]){
	    //             	    hasOuterSpace = false;
	    //             	    continue;
	    //             	}

	    //                 for (var pc = -cell_padding; pc <= cell_padding; pc++) {

	    //                     var checkCell = parseInt(cell) + pc;

	    //                     // Если текущей ячейки нет в шуме (левые и правые края карты), то считать что внешнего пространства нет
	    //                     if(!noise[floorLayer][checkRow][checkCell]){
	    //                         hasOuterSpace = false;
	    //                         continue;
	    //                     }

	    //                     // Если текущая ячейка уже занята
	    //                     if (objectsMatrix[floorLayer][checkRow][checkCell] !== 0) {
	    //                         hasOuterSpace = false;
	    //                         continue;
	    //                     }

	    //                     // Если фактор ячейки не попадает в диапазон
	    //                     if (noise[floorLayer][checkRow][checkCell] > factorMax || noise[floorLayer][checkRow][checkCell] < factorMin) {
	    //                         hasOuterSpace = false;
	    //                         continue;
	    //                     }

	    //                     excluded_in_this_plane.push({
	    //                         row: parseInt(checkRow), 
	    //                         cell: parseInt(checkCell),
	    //                         value : currentIndoorCell
	    //                     });
	    //                 }
	    //             }

	    //             if (hasOuterSpace) {
	                    
	    //                 generatedObjects++;

	                    
	    //                 // Добавить тротуары вокруг

     //            		if (districtId == Globals.DISTRICTS.UPTOWN.ID ){
     //            			var space_row_padding = row_padding + rand.intBetween(2, 5);
     //            			var space_cell_padding = cell_padding + rand.intBetween(2, 5);
     //            		}
     //            		else if (districtId == Globals.DISTRICTS.MIDTOWN.ID ){
     //            			var space_row_padding = row_padding + rand.intBetween(2, 4);
     //            			var space_cell_padding = cell_padding + rand.intBetween(2, 4);
     //            		}
     //            		else if (districtId == Globals.DISTRICTS.SUBURB.ID ){
     //            			var space_row_padding = row_padding + rand.intBetween(2, 3);
     //            			var space_cell_padding = cell_padding + rand.intBetween(2, 3);
     //            		}
	                   
	    //                 for (var pr = -space_row_padding; pr <= space_row_padding; pr++) {

	    //                 	var checkRow = row - pr;
     //                    	if(!noise[floorLayer][checkRow]){
     //                    	    continue;
     //                    	}

	    //                     for (var pc = -space_cell_padding; pc <= space_cell_padding; pc++) {
	                           
	    //                     	var checkCell = cell - pc;
	                        	
	    //                     	if(!noise[floorLayer][checkRow][checkCell]){
	    //                     	    continue;
	    //                     	}

	    //                        	// Выбор тайла
	    //                     	var tileId = 0;
                        		
     //                    		if (districtId == Globals.DISTRICTS.UPTOWN.ID )
     //                    			tileId = Globals.TILES.PAVEMENT.UPTOWN.ID
     //                    		else if (districtId == Globals.DISTRICTS.MIDTOWN.ID )
     //                    			tileId = Globals.TILES.PAVEMENT.MIDTOWN.ID
     //                    		else if (districtId == Globals.DISTRICTS.SUBURB.ID )
     //                    			tileId = Globals.TILES.PAVEMENT.SUBURB.ID

	    //                     	// Правило перезаписи ячейки
	    //                     	// Если текущая ячейка уже занята
	    //                     	if (objectsMatrix[floorLayer][checkRow][checkCell] > tileId) {
	    //                     	    continue;
	    //                     	}

	    //                        	// Если текущая ячейка уже занята в текущем объекте
	    //                        	if (excluded_in_this_plane.filter(function(item){ 
     //                                return item.row == checkRow && item.cell == checkCell && item.value > tileId;
     //                            }).length > 0 ) {
	    //                        		continue;
     //                            }

     //                            // Добавить случайность в тротуар
     //                            // if (districtId == Globals.DISTRICTS.SUBURB.ID) {
     //                            // 	if (rand.intBetween(0,100) < 1)
     //                            // 		tileId = Globals.TILES.ENTITIES.STEAM.ID
     //                            // }
                                

     //                        	excluded_in_this_plane.push({
     //                        	    row: checkRow,
     //                        	    cell: checkCell,
     //                        	    value : tileId
     //                        	});

	    //                     }
	    //                 }

	    //                 var floors = 1;
	    //                 // Добавить комнату со случайной этажностью
	    //                 if (districtId == Globals.DISTRICTS.UPTOWN.ID ){
	    //                 	floors = rand.intBetween(4,6);
	    //                 }
	    //                 else if (districtId == Globals.DISTRICTS.MIDTOWN.ID ){
	    //                 	floors = rand.intBetween(2,4);
	    //                 }
	    //                 else if (districtId == Globals.DISTRICTS.SUBURB.ID ){
	    //                 	floors = rand.intBetween(1,2);
	    //                 }

	    //                 var tempRoom = {
	    //                 	x : cell - cell_padding,
	    //                 	y : row - row_padding,
	    //                 	width : ( cell_padding * 2 ) + 1,
	    //                 	height : ( row_padding * 2 ) + 1,
	    //                 	districtId : districtId,
	    //                 	floors : floors
	    //                 }

	    //                 generatedRoomsData.push(tempRoom);
	                   

	    //                 // ОСНОВНАЯ ОТРИСОВКА
	    //                 for (var p = 0; p < excluded_in_this_plane.length; p++){
	    //                     var point = excluded_in_this_plane[p];
	    //                     objectsMatrix[floorLayer][point.row][point.cell] = point.value;
	    //                 }

	         
	    //             }
	    //         }
	    //     }	        
    	// }
    	setBlock(1, 0, 1, Globals.TILES.WALL.MIDTOWN.ID);


    	setBlock(1, 0, 1, Globals.TILES.WALL.MIDTOWN.ID);
    	setBlock(2, 0, 1, Globals.TILES.WALL.MIDTOWN.ID);
    	setBlock(3, 0, 1, Globals.TILES.WALL.MIDTOWN.ID);
    	setBlock(4, 0, 1, Globals.TILES.WALL.MIDTOWN.ID);
    	setBlock(5, 0, 1, Globals.TILES.WALL.MIDTOWN.ID);
    	setBlock(5, 0, 1, Globals.TILES.WALL.MIDTOWN.ID);
    	setBlock(6, 0, 1, Globals.TILES.WALL.MIDTOWN.ID);
    	setBlock(7, 0, 1, Globals.TILES.WALL.MIDTOWN.ID);
    	setBlock(8, 0, 1, Globals.TILES.WALL.MIDTOWN.ID);
    	setBlock(9, 0, 1, Globals.TILES.WALL.MIDTOWN.ID);
    	setBlock(10, 0, 1, Globals.TILES.WALL.MIDTOWN.ID);






    	// Генерация грязной земли и воды
    	// for(var n in noise){
	    // 	for (var row in noise[n]) {
		   //      for (var cell in noise[n][row]) {
		   //          var noiseValue = noise[n][row][cell];
		   //          var matrixValue = objectsMatrix[n][row][cell];

		   //          if (noiseValue <= factorMin && matrixValue < Globals.TILES.WALL.MIDTOWN.ID) {
		   //          	objectsMatrix[n][row][cell] = Globals.TILES.WALL.MIDTOWN.ID;
		   //          } else if (noiseValue >= (factorMax - 0.5) && matrixValue == 0) {
		   //          }	

		   //      }
		        
	    // 	}
    	// }

		if (false)
    	for (var r = 0; r < generatedRoomsData.length; r++) {
    		
    		let tempRoom = generatedRoomsData[r];

    		var row = tempRoom.y;
    		var cell = tempRoom.x; 

    		// СТЕНЫ

    		// Залить комнату цветом и получить будущие стены в границах заливки
           	var testLayer = objectsMatrix[floorLayer];
        	function testMatrix(x, y) {
		    	return testLayer[x][y];
			}
           	var testFlood = floodFill({
        		getter : testMatrix,
        		seed : [ row, cell ]
        	});

    		var wallId =  [0];
    		var wallAltId =  [0];

        	if (tempRoom.districtId == Globals.DISTRICTS.UPTOWN.ID ) {
        		wallId = [ Globals.TILES.WALL.UPTOWN.ID ];
        		wallAltId = [ Globals.TILES.WALL.UPTOWN_ALT.ID ];
        	}
        	else if (tempRoom.districtId == Globals.DISTRICTS.MIDTOWN.ID ) {
    			wallId = [ Globals.TILES.WALL.MIDTOWN.ID ];
    			wallAltId = [ Globals.TILES.WALL.MIDTOWN_ALT.ID ];
        	}
        	else if (tempRoom.districtId == Globals.DISTRICTS.SUBURB.ID ) {
        		wallId = [ 
        			Globals.TILES.WALL.SUBURB0.ID,
        			Globals.TILES.WALL.SUBURB1.ID,
        			Globals.TILES.WALL.SUBURB2.ID,
        			Globals.TILES.WALL.SUBURB3.ID,
        			Globals.TILES.WALL.SUBURB4.ID
    			];
        		wallAltId = wallId;
        	}

           	// Нарисовать стены соответвтующие району

           	for (var rI = row; rI < row + tempRoom.height; rI++) {
           		for (var cI = cell; cI < cell + tempRoom.width; cI++) {
	           		for (var level = 1; level <= (tempRoom.floors * 4); level++) {
	           			if ((cI == cell || cI == cell + tempRoom.width - 1) || (rI == row || rI == row + tempRoom.height - 1 )) {
								if ( level && (level % 4 === 0)) {

		        					objectsMatrix[level][rI][cI] =  wallAltId[Math.floor(Math.random()*wallAltId.length)];
		        				
		        				} else {

		        					objectsMatrix[level][rI][cI] =  wallId[Math.floor(Math.random()*wallId.length)];

		        				}				
	           			}


	    			}	
           		}
           		
           	}




        	// for (var i = 0; i < testFlood.boundaries.length; i++) {

        	// 		var floodRow = testFlood.boundaries[i][0];
        	// 		var floodCell = testFlood.boundaries[i][1];

        	// 		for (var level = 1; level <= (tempRoom.floors * 4); level++) {

    					// objectsMatrix[level][floodRow][floodCell] = wallAltId[Math.floor(Math.random()*wallAltId.length)];


        	// 			

        	// 		}
            		
         // 	}




			// Повторная генерация минимального тротуара вокруг дома

	    	var pavementId = 0;

	    	if (tempRoom.districtId == Globals.DISTRICTS.UPTOWN.ID ) {
	    		pavementId = Globals.TILES.PAVEMENT.UPTOWN.ID;
	    	}
	    	else if (tempRoom.districtId == Globals.DISTRICTS.MIDTOWN.ID ) {
	    		pavementId = Globals.TILES.PAVEMENT.MIDTOWN.ID;
	    	}
	    	else if (tempRoom.districtId == Globals.DISTRICTS.SUBURB.ID ) {
	    		pavementId = Globals.TILES.PAVEMENT.SUBURB.ID;
	    	}

    		// Если верхняя граница не за пределами карты
    		if (objectsMatrix[0][row - 1]) {
    			for (var cI = cell; cI < (cell + tempRoom.width + 1); cI++ ) {
    				if (objectsMatrix[0][row - 1][cI])
						objectsMatrix[0][row - 1][cI] = pavementId;
    			}
    		}

    		// Нижняя граница
    		if (objectsMatrix[0][row + tempRoom.height]) {
    			for (var cI = cell; cI < (cell + tempRoom.width + 1); cI++ ) {
    				if (objectsMatrix[0][row + tempRoom.height][cI])
						objectsMatrix[0][row + tempRoom.height][cI] = pavementId;
    			}
    		}

    		// Левая
    		if (objectsMatrix[0][row]) {
    			for (var rI = row; rI < (row + tempRoom.height); rI++ ) {
    				if (objectsMatrix[0][rI] && objectsMatrix[0][rI][cell])
					objectsMatrix[0][rI][cell] = pavementId;
    			}
    		}

    		// Правая
    		if (objectsMatrix[0][row]) {
    			for (var rI = row; rI < (row + tempRoom.height); rI++ ) {
    				if (objectsMatrix[0][rI] && objectsMatrix[0][rI][cell + tempRoom.width])
						objectsMatrix[0][rI][cell + tempRoom.width] = pavementId;
    			}
    		}


		}


    	// objectsMatrix[0][10][6] = 40;
    	// objectsMatrix[0][15][15] = 40;
    	// objectsMatrix[0][25][15] = 40;
    	// objectsMatrix[0][35][15] = 40;
    	// objectsMatrix[0][35][38] = 40;

    	// // objectsMatrix[1][4][4] = 0;
    	// objectsMatrix[1][3][4] = 0;
    	// objectsMatrix[2][3][4] = 0;

    	// objectsMatrix[2][5][4] = 0;




	    var end_time = new Date();

	    console.log("Generated with: " + ((end_time - start_time) / 1000) + "s");
	    
	    var result = {
	        'color' : objectOptions.color,
	        'matrix' : objectsMatrix
	    }

	    return result;
}
}


        