var PNGImage = require('pngjs-image');
var SimplexNoise = require('simplex-noise');
var generator = require('random-seed');
var WorldGenerator = require('./WorldGenerator');

var globals = require('./Globals');
var Globals = new globals();

var math3d = require('math3d');
var Quaternion = math3d.Quaternion;
var Vector3 = math3d.Vector3;

var MatrixExtensions = require('./../helpers/MatrixExtension');
var floodFill = require("n-dimensional-flood-fill");
var PF = require('pathfinding');
// Дома
var buildings = [
    // require("./buildings/square8x8/Square8x8"),
    require("./buildings/square10x10/Square10x10")
]

module.exports = class World {

    lerp (a,b,c){
        return(1-c)*a+c*b
    }
    
    constructor (parent, x, y, z) {
        
        this.parent = parent;
        this.x = x;
        this.y = y;
        this.z = z;

        var seed = parent.getTitle()
        // var seed = Math.random();
        this.rand = generator.create(seed);
        this.seed = seed;
        
        this.generateNoise();
        this.generateMatrix();
        this.generateLightmap();

        this.generateGround();
        this.generateBuilding();



        var lights = [
        ]

        // this.setBlock(13, 3, 16, 401);
        // this.setBlock(23, 3, 16, 401);
        // this.setBlock(33, 3, 16, 401);
        // this.setBlock(43, 3, 16, 401);
        // this.setBlock(13, 4, 16, 321);
        // this.setBlock(23, 4, 16, 321);
        // this.setBlock(33, 4, 16, 321);
        // this.setBlock(43, 4, 16, 321);


        this.setBlock(12, 3, 16, 401);
        this.setBlock(12, 4, 16, 32);
        this.setBlock(13, 4, 16, 32);
        this.setBlock(14, 4, 16, 32);
        this.setBlock(14, 3, 16, 321);
        this.setBlock(14, 2, 16, 321);
        this.setBlock(14, 1, 16, 321);

        for (var yI = 0; yI < this.matrix.length; yI++) {
            for (var xI = 0; xI < this.matrix[yI].length; xI++) {
                for (var zI = 0; zI < this.matrix[yI][xI].length; zI++) {
                    if (this.matrix[yI][xI][zI] === 401) {
                        lights.push({
                            x: xI,
                            y: yI,
                            z: zI
                        })
                    }
                }
            }
        }


        
        // this.setLightLevel(lightPos.x, lightPos.y, lightPos.z, 1);
        // this.setLightLevel(lightPos.x, lightPos.y -, lightPos.z, 1);
        // this.setLightLevel(lightPos.x - 1, lightPos.y, lightPos.z, 1);

        for (var lI = 0; lI < lights.length; lI++) {

            var lightPos = lights[lI];

            var lightPower = 4;

            this.setLightLevel(lightPos.x, lightPos.y, lightPos.z, lightPower);  


            // Горизонталь

            // Создадим куб и направим лучи во углы
            // Углы куба
            var cubeCorners = [];
            
            for (var yI = (lightPos.y + -lightPower); yI <= (lightPos.y + lightPower); yI++) {
                for (var xI = (lightPos.x + -lightPower); xI <= (lightPos.x + lightPower); xI++) {
                    for (var zI = (lightPos.z + -lightPower); zI <= (lightPos.z + lightPower); zI++) {
                        cubeCorners.push(new Vector3(xI, yI, zI));
                    }
                }
            }


            for (var cI = 0; cI < cubeCorners.length; cI++) {
                
                var corner = cubeCorners[cI];
                var currentStep = new Vector3(lightPos.x, lightPos.y, lightPos.z);
                
                var isStopped = false;

                var currentPower = lightPower;

                while (!currentStep.equals(corner) && !isStopped && currentPower > 0) { 

                    var newx = ( currentStep.x == corner.x ? currentStep.x : parseInt(this.lerp(currentStep.x, corner.x, Math.abs(1 / ( currentStep.x - corner.x )))));
                    var newy = ( currentStep.y == corner.y ? currentStep.y : parseInt(this.lerp(currentStep.y, corner.y, Math.abs(1 / ( currentStep.y - corner.y )))));
                    var newz = ( currentStep.z == corner.z ? currentStep.z : parseInt(this.lerp(currentStep.z, corner.z, Math.abs(1 / ( currentStep.z - corner.z )))));

                    if (!this.isSolid(newx, newy, newz)) {

                        var diff = 0;

                        if (Math.abs(newx) - Math.abs(currentStep.x))
                            diff++;

                        if (Math.abs(newy) - Math.abs(currentStep.y))
                            diff++;

                        if (Math.abs(newz) - Math.abs(currentStep.z))
                            diff++;

                        if (diff == 1) {
                            currentPower--;
                        } else if (diff == 2) {
                            currentPower -= 2;
                        } else {
                            currentPower -= 3;
                        }

                        

                        currentStep = new Vector3(newx, newy, newz);
                        this.setLightLevel(currentStep.x, currentStep.y, currentStep.z, currentPower);  

                    } else {

                        isStopped = true;

                    }

                    // currentStep = new Vector3(newx, newy, newz);
                        // console.log("    current lerp x: " + currentStep.x + " y: " + currentStep.y + " z: " + currentStep.z);
                    // this.setLightLevel(currentStep.x, currentStep.y, currentStep.z, 3);  

                    

                }

        
                


            }


        }

        

        

        // box
        // this.setBlock(10, 0, 6, 70);
        

        // this.setBlock(10, 0, 6, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 1, 4, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 1, 6, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 2, 4, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 2, 5, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 2, 6, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(11, 2, 6, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(11, 1, 6, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(11, 0, 6, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(12, 0, 6, Globals.TILES.WALL.MIDTOWN.ID);

        // this.setBlock(11, 0, 4, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(11, 0, 6, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(11, 1, 4, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(11, 1, 6, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(11, 2, 4, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(11, 2, 5, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(11, 2, 6, Globals.TILES.WALL.MIDTOWN.ID);


        // this.setBlock(10, 0, 3, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 0, 2, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 0, 0, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 1, 2, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 1, 1, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 1, 0, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 2, 2, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 2, 1, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 2, 0, Globals.TILES.WALL.MIDTOWN.ID);
        
        // this.setBlock(10, 3, 2, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 3, 4, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(10, 3, 3, Globals.TILES.WALL.MIDTOWN.ID);



        // this.setBlock(4, 0, 4, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(4, 0, 5, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(4, 0, 6, Globals.TILES.WALL.MIDTOWN.ID);

        // var cX = 8;
        // for (var cY = 0; cY < 5; cY++){
        //     for (var cZ = 12; cZ < 25; cZ++){
        //         this.setBlock(cX, cY, cZ, Globals.TILES.WALL.MIDTOWN.ID);
        //     }   
        //     cX++;
        // }

        // this.setBlock(5, 0, 5, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(5, 1, 5, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(6, 0, 5, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(7, 0, 5, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(6, 0, 6, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(5, 0, 6, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(5, 0, 7, Globals.TILES.WALL.MIDTOWN.ID);

        // this.setBlock(4, 0, 5, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(4, 0, 6, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(3, 0, 5, Globals.TILES.WALL.MIDTOWN.ID);

        // this.setBlock(5, 0, 3, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(5, 0, 4, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(6, 0, 4, Globals.TILES.WALL.MIDTOWN.ID);
        // this.setBlock(4, 0, 4, Globals.TILES.WALL.MIDTOWN.ID);
        

        // for (var cY = 0; cY < 5; cY++){
        //     this.setBlock(9, cY, 11, Globals.TILES.WALL.MIDTOWN.ID);
        //     this.setBlock(8, cY, 11, Globals.TILES.WALL.MIDTOWN.ID);
        //     this.setBlock(7, cY, 11, Globals.TILES.WALL.MIDTOWN.ID);
        //     this.setBlock(6, cY, 11, Globals.TILES.WALL.MIDTOWN.ID);
        //     this.setBlock(5, cY, 11, Globals.TILES.WALL.MIDTOWN.ID);
        //     this.setBlock(4, cY, 11, Globals.TILES.WALL.MIDTOWN.ID);
        //     this.setBlock(3, cY, 11, Globals.TILES.WALL.MIDTOWN.ID);
        //     this.setBlock(2, cY, 11, Globals.TILES.WALL.MIDTOWN.ID);
        //     this.setBlock(1, cY, 11, Globals.TILES.WALL.MIDTOWN.ID);
        //     this.setBlock(0, cY, 11, Globals.TILES.WALL.MIDTOWN.ID);

        //     for (var cZ = 12; cZ < 20; cZ++){
        //         this.setBlock(5, cY, cZ, Globals.TILES.WALL.MIDTOWN.ID);
                
        //     }

        //     for (var cZ = 11; cZ < 20; cZ++){
        //         this.setBlock(10, cY, cZ, Globals.TILES.WALL.MIDTOWN.ID);
        //     }

        //     for (var cX = 5; cX < 10; cX++){
        //         this.setBlock(cX, cY, 19, Globals.TILES.WALL.MIDTOWN.ID);
        //     }

        // }
        // this.generateImage()
    }
    getBlock(x, y, z) {
        if (y in this.matrix)
            if (x in this.matrix[y])
                if (z in this.matrix[y][x])
                    return this.matrix[y][x][z]
        return -1;
    }
    isSolid(x, y, z) {
        if (y in this.matrix)
            if (x in this.matrix[y])
                if (z in this.matrix[y][x])
                    return ( this.matrix[y][x][z] !== 0 ) 
        return true;
    }
    setBlock(x, y, z, value) {
        if (y in this.matrix)
            if (x in this.matrix[y])
                if (z in this.matrix[y][x])
                    this.matrix[y][x][z] = value;
    }
    setLightLevel(x, y, z, value) {
        if (y in this.lightmap)
            if (x in this.lightmap[y])
                if (z in this.lightmap[y][x]) {
                    if (value > this.lightmap[y][x][z])
                        this.lightmap[y][x][z] = value;
                }
    }
    generateMatrix() {
        this.matrix = function(x, y, z) {
            var layers = new Array(y);
            for(var layer = 0; layer < y; layer++){
                var arr = [];
                for(var i=0; i < x; i++){
                    arr.push([]);
                    arr[i].push( new Array(z));
                    for(var j=0; j < z; j++){
                        arr[i][j] = 0;
                    }
                }
                layers[layer] = arr;
            }
            return layers;
        }(this.x, this.y, this.z);
    }
    generateGround(){
        for(var i=0; i < this.matrix[0].length; i++){
            for(var j=0; j < this.matrix[0][i].length; j++){
                this.setBlock(i, 0, j, Globals.TILES.GROUND.DEFAULT.ID);
            }
        }
    }
    generateLightmap() {
        this.lightmap = function(x, y, z) {
            var layers = new Array(y);
            for(var layer = 0; layer < y; layer++){
                var arr = [];
                for(var i=0; i < x; i++){
                    arr.push([]);
                    arr[i].push( new Array(z));
                    for(var j=0; j < z; j++){
                        arr[i][j] = 0;
                    }
                }
                layers[layer] = arr;
            }
            return layers;
        }(this.x, this.y, this.z);

    }
    generateNoise() {
        this.noise = function(seed, x, z){
            var simplex = new SimplexNoise(seed);
            var arr = [];
            for(var i=0; i < x; i++){
                arr.push([]);
                arr[i].push( new Array(x));
                for(var j=0; j < z; j++){
                    var r = simplex.noise3D(i / (x * 0.55), j / (z * 0.55), 1);
                    arr[i][j] = r * 0.5 + 0.5;
                }
            }
            return arr;
        }(this.seed, this.x, this.z);
    }
    getRandomArbitrary(min, max) {
      return Math.random() * (max - min) + min;
    }
    generateBuilding() {

        
        for (var bX = 0; bX < 1; bX++ ) {


            for (var bZ = 0; bZ < 1; bZ++) {


                var item = buildings[Math.floor(Math.random()*buildings.length)];

                var building = new item();
                var matrixBuilding = building.getBaseMatrix();


                var lastHeight = 0;
                var rand = this.getRandomArbitrary(17,20);

                var randX = 20;
                var randZ = 20;

                var paddingPos = 5;

                for (var layer = 0; layer < matrixBuilding.length; layer++) {
                    for (var rowIndex = 0; rowIndex < matrixBuilding[layer].length; rowIndex++) {
                     for (var cellIndex = 0; cellIndex < matrixBuilding[layer][rowIndex].length; cellIndex++) {
                         var value = matrixBuilding[layer][rowIndex][cellIndex];
                         this.setBlock(rowIndex + (randX * bX) + 2, layer + 1, cellIndex + (randZ * bZ) + 4, value);
                     }
                    }
                }
                lastHeight += matrixBuilding.length + 1;

                for (var floor = 0; floor < this.getRandomArbitrary(0,7); floor++) {
                    var floorMatrix = building.getFloorMatrix();
                    for (var layer = 0; layer < floorMatrix.length; layer++) {
                        for (var rowIndex = 0; rowIndex < floorMatrix[layer].length; rowIndex++) {
                         for (var cellIndex = 0; cellIndex < floorMatrix[layer][rowIndex].length; cellIndex++) {
                             var value = floorMatrix[layer][rowIndex][cellIndex];
                             this.setBlock(rowIndex + (randX * bX) + 2, layer + lastHeight, cellIndex + (randZ * bZ) + 4, value);
                         }
                        }
                    }
                    lastHeight += floorMatrix.length
                }
            }
        }
    }
    generateImage () {

        let multiplier = 4;
        var image = PNGImage.createImage(this.z * multiplier, this.x * multiplier * this.y);

        // Заполнение всего изображения черным прямоугольником
        image.fillRect(0, 0, this.z * multiplier, this.x * multiplier * this.y, { 
            red: 1, 
            green: 1, 
            blue: 1,
            alpha : 255
        });

        // Отрисовка шума на уровне 0
        for(var xI = 0; xI < this.noise.length; xI++){

           var xRow = this.noise[xI];
           for (var zI = 0; zI < xRow.length; zI++) {
                
                var value = xRow[zI];
                var scaleGray = ( 255 * value );

                var color =  { 
                    red: scaleGray, 
                    green: scaleGray, 
                    blue: scaleGray,
                    alpha : 255
                };

                var imgX = zI;
                var imgZ = xI;

                image.fillRect(
                    ( imgX * multiplier ),
                    imgZ * multiplier,
                    1 * multiplier,
                    1 * multiplier,
                    color
                );

           }
        }


        // Рисуем всю матрицу
        for (var yI = 0; yI < this.matrix.length; yI++) {
            var layer = this.matrix[yI];
            for (var xI = 0; xI < layer.length; xI++) {
                var xRow = layer[xI];
                for (var zI = 0; zI < xRow.length; zI++) {
                    var value = xRow[zI];
                    
                    if (value == Globals.TILES.WALL.MIDTOWN.ID) {
                        var color = { 
                            red: 0, 
                            green: 150, 
                            blue: 0,
                            alpha : 255
                        }
                    }

                    if (value == 22) {
                        var color = { 
                            red: 0, 
                            green: 0, 
                            blue: 0,
                            alpha : 255
                        }
                    }

                    if (value && color) {

                       var imgX = zI;
                       var imgZ = ( layer.length - xI - 1 );

                       image.fillRect(
                           ( imgX * multiplier ),
                           ( imgZ * multiplier ) + ((layer.length * multiplier) * yI),
                           1 * multiplier,
                           1 * multiplier, 
                           color
                       ); 
                    }


                }
            }
        }

        // var gridMatrix = [];
        // var nonWalkable = [30, 31, 32, 33];
        // for (var i = 0; i < this.objects[0].matrix[1].length; i++) {
        //     var matrixRow = this.objects[0].matrix[1][i];
        //     var newRow = matrixRow.map(function(item){
        //         return nonWalkable.includes(item) ? 1 : 0;
        //     });
        //     gridMatrix.push(newRow);
        // }

        // var grid = new PF.Grid(gridMatrix); 
        // var finder = new PF.AStarFinder({
        //         allowDiagonal: true
        // });
        // var path = finder.findPath(0,8, this.width - 1, this.height / 2, grid);
        //  for (var stepIndex in path) {
        //     var step = path[stepIndex];
        //     image.fillRect(
        //         (step[0] * multiplier),
        //         step[1] * multiplier,
        //         1 * multiplier,
        //         1 * multiplier,
        //         { 
        //             red: 255, 
        //             green: 0, 
        //             blue: 0,
        //             alpha : 255
        //         }
        //     );
        // }
       
        
        var filename = './images/output.png';

        image.writeImage(filename, function (err) {
            if (err) throw err;
            console.log('Minimap written to file');
        });

    }
    getNoise() {
        return this.noise;
    }
    getWidth() {
        return this.width;
    }
    getHeight() {
        return this.height;
    }
    getMatrix () {
        return this.matrix;
    }
    getLightmap () {
        return this.lightmap;
    }
    getObjects () {
        return this.objects;
    }
}