(function() {
  var DOOR = (function() {
    
    class Door {
        constructor() {
        	this.id = "test"
        }
    }

    return new Door();
  })();

  if (typeof module !== 'undefined' && typeof module.exports !== 'undefined')
    module.exports = Door;
  else
    window.Door = Door;
})();