var math3d = require('math3d');
var Vector3 = math3d.Vector3;

module.exports = class Actor {
    constructor(id, speed, position) {
        this.id = id;
        this.speed = speed;
        this.position = new Vector3(position.x, position.y, position.z);
        this.velocity = new Vector3(0,0,0);
    }

    update (delta) {

        var newPosition = new Vector3(this.position.x, this.position.y, this.position.z);

        // Обновление положения согласно скорости и дельте
        if (this.velocity.x > 0) {
            newPosition.x += (this.speed / delta);
        }
        if (this.velocity.x < 0) {
            newPosition.x -= (this.speed / delta);
        }

        if (this.velocity.z > 0) {
            newPosition.z += (this.speed / delta);
        }
        if (this.velocity.z < 0) {
            newPosition.z -= (this.speed / delta);
        }

        this.position = newPosition;
        this.velocity = new Vector3(0,0,0);

    }


}