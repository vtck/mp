var PF = require('pathfinding');
var math3d = require('math3d');
var Quaternion = math3d.Quaternion;
var Vector3 = math3d.Vector3;


module.exports = class Player {
    constructor(matrix, id, name, socket) {
        this.matrix = matrix;
        this.id = id;
        this.name = name;
        this.socket = socket;
	    
	    this.speed = 6;
        this.gravity = 6;
        this.xlen = 0.76;
        this.ylen = 1.4;
        this.zlen = 0.76;

        this.grounded = false;
        this.jumping = false;
        this.jumpEnergy;
        this.fallingDistance = 0;

        this.direction = new Vector3(0,0,0);
        this.rotation = new Vector3(0,0,0);
        this.pitch = 0;

        this.lastPos = new Vector3(0,0,0);
        this.velocity = new Vector3(0,0,0);
        // this.position = new Vector3(5,10,10);
        // this.position = new Vector3(12,10,15);
	    this.position = new Vector3(13,20,20);
     //    this.position = new Vector3(
	    // 	this.getRandomArbitrary(8,12),
	    // 	10,
	    // 	this.getRandomArbitrary(8,14)
	    // )
    }
    getBlock(x, y, z) {
        if (y in this.matrix)
            if (x in this.matrix[y])
                if (z in this.matrix[y][x])
                    return this.matrix[y][x][z]
        return -1;
    }
    isSolid(x, y, z) {
        if (y in this.matrix)
            if (x in this.matrix[y])
                if (z in this.matrix[y][x])
                    return ( this.matrix[y][x][z] !== 0 ) 
        return true;
    }
    doorNearby(x, y, z) {
        var doorsNearby = [];
        // console.log(x + " " + y + " " + z);
        return doorsNearby;

    }
    getRandomArbitrary(min, max) {
      return Math.random() * (max - min) + min;
    }
    update (delta) {


		var x = this.position.x;
		var y = this.position.y;
		var z = this.position.z;

        var bottomPlanePos = this.position.y - (this.ylen / 2) + 0.45; 
        var bottomGrid = [
            new Vector3(
                Math.floor(x + 0.5 + (this.xlen / 2)),
                Math.floor(bottomPlanePos),
                Math.floor(z + 0.5 + (this.zlen / 2))
            ),
            new Vector3(
                Math.floor(x + 0.5 - (this.xlen / 2)),
                Math.floor(bottomPlanePos),
                Math.floor(z + 0.5 + (this.zlen / 2))
            ),
            new Vector3(
                Math.floor(x + 0.5 + (this.xlen / 2)),
                Math.floor(bottomPlanePos),
                Math.floor(z + 0.5 - (this.zlen / 2))
            ),
            new Vector3(
                Math.floor(x + 0.5 - (this.xlen / 2)),
                Math.floor(bottomPlanePos),
                Math.floor(z + 0.5 - (this.zlen / 2))
            ),
        ];


        this.grounded = false;
        for (var i = 0; i < bottomGrid.length; i++) {
            var point = bottomGrid[i];
            if(this.isSolid(point.x, point.y, point.z)){
                this.grounded = true;
                this.fallingDistance = 0;
                this.jumping = false;
            }
        }


        if(!this.grounded) {
            if (!this.jumpEnergy > 0) {
                this.fallingDistance += 0.001;
                y -= (( this.gravity / delta ) + this.fallingDistance );    
            }
            
        }


        var middlePlanePos = this.position.y + 0.5;
        var topPlanePos    = this.position.y + (this.ylen / 2) + 0.5;

        

        var v1 = new Vector3(this.velocity.x, 0, this.velocity.z);
        var q1 = Quaternion.Euler(
            this.rotation.x == 0 ? 0 : - 180, 
            this.rotation.x == 0 ? (( this.rotation.y * 180 / Math.PI ) - 270) : - (( this.rotation.y * 180 / Math.PI ) + 270), 
            this.rotation.z == 0 ? 0 : - 180,
        );
        var a = q1.mulVector3(v1); 

        var rotatedVelocity = {
            x : a.x,
            y : a.y,
            z : a.z
        }

        if (rotatedVelocity.x !== 0) {

            if (rotatedVelocity.x > 0){
                var nextX = (x + (rotatedVelocity.x / delta) * this.speed);

            }
            else if (rotatedVelocity.x < 0 ) {
                var nextX = (x - ( - rotatedVelocity.x / delta) * this.speed);
            }

            var xGrid = [
                new Vector3(
                    Math.floor(nextX + 0.5 + (this.xlen / 2)),
                    Math.floor(bottomPlanePos + 0.1),
                    Math.floor(z + 0.5 + (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(nextX + 0.5 - (this.xlen / 2)),
                    Math.floor(bottomPlanePos + 0.1),
                    Math.floor(z + 0.5 + (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(nextX + 0.5 + (this.xlen / 2)),
                    Math.floor(bottomPlanePos + 0.1),
                    Math.floor(z + 0.5 - (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(nextX + 0.5 - (this.xlen / 2)),
                    Math.floor(bottomPlanePos + 0.1),
                    Math.floor(z + 0.5 - (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(nextX + 0.5 + (this.xlen / 2)),
                    Math.floor(middlePlanePos),
                    Math.floor(z + 0.5 + (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(nextX + 0.5 - (this.xlen / 2)),
                    Math.floor(middlePlanePos),
                    Math.floor(z + 0.5 + (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(nextX + 0.5 + (this.xlen / 2)),
                    Math.floor(middlePlanePos),
                    Math.floor(z + 0.5 - (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(nextX + 0.5 - (this.xlen / 2)),
                    Math.floor(middlePlanePos),
                    Math.floor(z + 0.5 - (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(nextX + 0.5 + (this.xlen / 2)),
                    Math.floor(topPlanePos - 0.1),
                    Math.floor(z + 0.5 + (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(nextX + 0.5 - (this.xlen / 2)),
                    Math.floor(topPlanePos - 0.1),
                    Math.floor(z + 0.5 + (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(nextX + 0.5 + (this.xlen / 2)),
                    Math.floor(topPlanePos - 0.1),
                    Math.floor(z + 0.5 - (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(nextX + 0.5 - (this.xlen / 2)),
                    Math.floor(topPlanePos - 0.1),
                    Math.floor(z + 0.5 - (this.zlen / 2))
                ),
            ];

            var touchedPoints = 0;

            for (var i = 0; i < xGrid.length; i++) {
                var point = xGrid[i];
                if (this.isSolid(point.x, point.y, point.z)) {
                    touchedPoints++;
                }
            }

            if (touchedPoints == 0)
                x = nextX;
            
        }

        if (rotatedVelocity.z !== 0) {

            if (rotatedVelocity.z > 0) {
                var nextZ = (z + (rotatedVelocity.z / delta) * this.speed);
            }
            else if (rotatedVelocity.z < 0 ) {
                var nextZ = (z - ( - rotatedVelocity.z / delta) * this.speed);
            }

            var zGrid = [
                new Vector3(
                    Math.floor(x + 0.5 + (this.xlen / 2)),
                    Math.floor(bottomPlanePos + 0.1),
                    Math.floor(nextZ + 0.5 + (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(x + 0.5 - (this.xlen / 2)),
                    Math.floor(bottomPlanePos + 0.1),
                    Math.floor(nextZ + 0.5 + (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(x + 0.5 + (this.xlen / 2)),
                    Math.floor(bottomPlanePos + 0.1),
                    Math.floor(nextZ + 0.5 - (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(x + 0.5 - (this.xlen / 2)),
                    Math.floor(bottomPlanePos + 0.1),
                    Math.floor(nextZ + 0.5 - (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(x + 0.5 + (this.xlen / 2)),
                    Math.floor(middlePlanePos),
                    Math.floor(nextZ + 0.5 + (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(x + 0.5 - (this.xlen / 2)),
                    Math.floor(middlePlanePos),
                    Math.floor(nextZ + 0.5 + (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(x + 0.5 + (this.xlen / 2)),
                    Math.floor(middlePlanePos),
                    Math.floor(nextZ + 0.5 - (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(x + 0.5 - (this.xlen / 2)),
                    Math.floor(middlePlanePos),
                    Math.floor(nextZ + 0.5 - (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(x + 0.5 + (this.xlen / 2)),
                    Math.floor(topPlanePos - 0.1),
                    Math.floor(nextZ + 0.5 + (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(x + 0.5 - (this.xlen / 2)),
                    Math.floor(topPlanePos - 0.1),
                    Math.floor(nextZ + 0.5 + (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(x + 0.5 + (this.xlen / 2)),
                    Math.floor(topPlanePos - 0.1),
                    Math.floor(nextZ + 0.5 - (this.zlen / 2))
                ),
                new Vector3(
                    Math.floor(x + 0.5 - (this.xlen / 2)),
                    Math.floor(topPlanePos - 0.1),
                    Math.floor(nextZ + 0.5 - (this.zlen / 2))
                ),
            ];

            var touchedPoints = 0;

            for (var i = 0; i < zGrid.length; i++) {
                var point = zGrid[i];
                if (this.isSolid(point.x, point.y, point.z)) {
                    touchedPoints++;
                }
            }

            if (touchedPoints == 0)
                z = nextZ;
            
        }
        

        if (this.velocity.y > 0 || this.jumpEnergy > 0) {
            if(!this.jumping) {
                this.jumping = true;
                this.jumpEnergy = 15;    
            }
            if (this.jumpEnergy < 0.4)
                this.jumpEnergy = 0;

            if (this.jumpEnergy > 0) {
                var nextY = (y + (this.jumpEnergy / delta));  

                var nextYplanePos  = nextY + (this.ylen / 2) + 0.5;  

                var yGrid = [

                    new Vector3(
                        Math.floor(x + 0.5 + (this.xlen / 2)),
                        Math.floor(nextYplanePos),
                        Math.floor(z + 0.5 + (this.zlen / 2))
                    ),
                    new Vector3(
                        Math.floor(x + 0.5 - (this.xlen / 2)),
                        Math.floor(nextYplanePos),
                        Math.floor(z + 0.5 + (this.zlen / 2))
                    ),
                    new Vector3(
                        Math.floor(x + 0.5 + (this.xlen / 2)),
                        Math.floor(nextYplanePos),
                        Math.floor(z + 0.5 - (this.zlen / 2))
                    ),
                    new Vector3(
                        Math.floor(x + 0.5 - (this.xlen / 2)),
                        Math.floor(nextYplanePos),
                        Math.floor(z + 0.5 - (this.zlen / 2))
                    ),
                ];

                var touchedPoints = 0;

                for (var i = 0; i < yGrid.length; i++) {
                    var point = yGrid[i];
                    if (this.isSolid(point.x, point.y, point.z)) {
                        touchedPoints++;
                    }
                }

                if (touchedPoints == 0) {
                    y = nextY;
                    this.jumpEnergy = this.jumpEnergy * 0.9;
                } else {
                    this.jumpEnergy = 0;
                }                
            }
           
        }

        // if (this.lastPos !== new Vector3(x, y, z) && this.grounded){
        //     this.doorNearby(Math.round(x), Math.round(y), Math.round(z));   
        // }

        if (y < 0 ) y = 0;
    
        this.lastPos = new Vector3(x,y,z);
        this.velocity = new Vector3(0,0,0);


        // this.position.translate(this.velocity)

        this.position = new Vector3(x,y,z);
    }
}