(function() {
  var Globals = (function() {
    
    var Globals = function() {
       return {
                TILES : {
                  EMPTY : 1,
                  GROUND : {
                    DEFAULT  : { 
                      ID : 2,
                      TEXTURE : "textures/dirt.png"
                    }
                  },
                  PAVEMENT : {
                    SUBURB   : {
                      ID : 11,
                      TEXTURE : "textures/pavement_suburb_4colors.png"
                    },
                    MIDTOWN  : {
                      ID : 12,
                      TEXTURE : "textures/pavement_midtown_4colors.png"

                    },
                    UPTOWN   : {
                      ID : 13,
                      TEXTURE : "textures/pavement_uptown_4colors.png"
                    }
                  },
                  INDOOR : {
                    SUBURB   : { 
                        ID : 21,
                        TEXTURE : "textures/floor.png" 
                    },
                    MIDTOWN  : { 
                        ID : 22,
                        FLOOR_TEXTURE : "textures/indoor_midtown_floor_1.png",
                        WALL_TEXTURE : "textures/wall_suburb_1_0.png" 
                    },
                    UPTOWN   : { 
                        ID : 23,
                        TEXTURE : "textures/floor.png"
                    }
                  },
                  WALL : {
                    SUBURB0    : {
                        ID : 31, 
                        TEXTURE : "textures/wall_suburb_1_0.png"
                    },
                    SUBURB1    : {
                        ID : 311, 
                        TEXTURE : "textures/wall_suburb_1_1.png"
                    },
                    SUBURB2    : {
                        ID : 312, 
                        TEXTURE : "textures/wall_suburb_1_2.png"
                    },
                    SUBURB3    : {
                        ID : 313, 
                        TEXTURE : "textures/wall_suburb_1_3.png"
                    },
                    SUBURB4    : {
                        ID : 314, 
                        TEXTURE : "textures/wall_suburb_1_4.png"
                    },
                    MIDTOWN    : { 
                      ID : 32, 
                      TEXTURE : "textures/brick_4colors.jpg"
                    },
                    MIDTOWN_ALT: { 
                      ID : 321, 
                      TEXTURE : "textures/wall_8colors.png"
                    },
                    UPTOWN     : { 
                      ID : 33, 
                      TEXTURE : "textures/wall_uptown_1.png"
                    },
                    UPTOWN_ALT : { 
                      ID : 331, 
                      TEXTURE : "textures/wall_uptown_2.png"
                    }
                  },
                  ENTITIES : {
                    LIGHT : {
                        ID : 401
                    },
                    STREETLIGHT : {
                      DEFAULT : { 
                        ID : 40,
                        TEXTURE_PILLAR : "textures/metal.jpg",
                      }
                    },
                    STEAM : {
                      ID : 50
                    },
                    DOOR : {
                      DEFAULT : {
                        ID : 60  
                      },
                      SUBURB : {
                        ID : 61  
                      }
                    },
                    ELECTROBOX : {
                      DEFAULT : {
                        ID : 70  
                      },
                      SUBURB : {
                        ID : 71  
                      }
                    },
                    GLASS : {
                      ID : 80,
                      TEXTURE : "textures/glass.jpg"
                    }
                  }
                },
                DISTRICTS : {
                    UPTOWN : {
                      ID : 1
                    },
                    MIDTOWN : {
                      ID : 2
                    },
                    SUBURB : {
                      ID : 3
                    }
                }
              }
    };

    return Globals;
  })();

  if (typeof module !== 'undefined' && typeof module.exports !== 'undefined')
    module.exports = Globals;
  else
    window.Globals = Globals;
})();