var World = require('./World');

module.exports = class Room {
  constructor(title, x, y, z) {
    this.title = title;
    this.world = new World(this, x, y, z);
    this.players = [];
  }
  getTitle() {
    return this.title;
  }
  getWorld() {
  	return this.world;
  }
  getPlayers() {
  	return this.players;
  }
}