const Actor = require('./Actor.js')

var PF = require('pathfinding');
var math3d = require('math3d');
var Vector3 = math3d.Vector3;

module.exports = class BaseMob extends Actor{
    constructor(room, id, position) {
        super(id, 3, position);
        this.room = room;
        this.path = null;
        this.routesQueue = [
            [1, 20]
        ]
    }

    makePath(w, h){
        if (process.env.VERBOSE_MOB_INFO == 1 ) console.log(this.id + " is making path..");
        var gridMatrix = [];

        var nonWalkable = [30, 31, 32, 33];
        for (var i = 0; i < this.room.world.objects[0].matrix[1].length; i++) {
            var matrixRow = this.room.world.objects[0].matrix[1][i];
            var newRow = matrixRow.map(function(item){
                return nonWalkable.includes(item) ? 1 : 0;
            });
            gridMatrix.push(newRow);
        }

        var grid = new PF.Grid(gridMatrix); 
        var finder = new PF.AStarFinder({
            allowDiagonal: true,
            dontCrossCorners: true
        });
        var path = finder.findPath(this.position.x, this.position.z, w, h, grid);

        if (process.env.VERBOSE_MOB_INFO == 1 ) console.log("Path maked! " + path.length + " cells.");
        
        function indexOfArray(val, array) {
          var
            hash = {},
            indexes = {},
            i, j;
          for(i = 0; i < array.length; i++) {
            hash[array[i]] = i;
          }
          return (hash.hasOwnProperty(val)) ? hash[val] : -1;
        }

        if (process.env.VERBOSE_MOB_PATH == 1 ) {
            console.log(this.id + ": новый маршрут проложен!");
            for (var i = 0; i < gridMatrix.length; i++) {
                    console.log(gridMatrix[i].map(function(item, k){
                        return indexOfArray([k, i], path) > 0 ? "*" : (item == 1 ? 1 : " ");
                    }, { path : path, this : this }).join(""));                
            }
        }
        this.path = path;
    }

    

    update (delta) {

        if (process.env.VERBOSE_MOB_INFO == 1 )console.log("----- Mob update starts!");
        if (process.env.VERBOSE_MOB_INFO == 1 )console.log('velocity x ' + this.velocity.x + ' z ' + this.velocity.z);

        // Обновление положения согласно скорости и дельте

        if (this.velocity.x > 0) {
            this.position.x += (this.speed / delta);
        }
        if (this.velocity.x < 0) {
            this.position.x -= (this.speed / delta);
        }

        if (this.velocity.z > 0) {
            this.position.z += (this.speed / delta);
        }
        if (this.velocity.z < 0) {
            this.position.z -= (this.speed / delta);
        }

        // Сброс скорости
        this.velocity = new Vector3(0,0,0);

        // Поиск следующего движения

        // Если нет текущего маршрута, или текущий маршрут пустой
        if (!this.path || !this.path.length) {
            
            // Добавить в очередь маршрутов новую цель
            if (this.routesQueue.length == 0) {

                var newTarget = {};

                for (var attempt = 0; attempt < 200; attempt++) {
                    var attemptX = this.room.world.rand.intBetween(0, 59);
                    var attemptY = this.room.world.rand.intBetween(0, 59);

                    var attemptPoint = this.room.world.objects[0].matrix[0][attemptX][attemptY];

                    if ( attemptPoint == 11 ) {
                        this.routesQueue.push([ attemptX, attemptY ])
                        break;  
                    }

                }

                if (process.env.VERBOSE_MOB_INFO == 1 )console.log("new path maked with " + attempt + " attempts");


            //      for (var i = 0; i < this.room.world.objects[0].matrix[1].length; i++) {
            //     var matrixRow = this.room.world.objects[0].matrix[1][i];
            //     var newRow = matrixRow.map(function(item){
            //         return nonWalkable.includes(item) ? 1 : 0;
            //     });
            //     gridMatrix.push(newRow);
            // }

            }

            


            // Взять следующую цель
            var nextPath = this.routesQueue.shift();
            // Если цель найдена, проложить маршрут
            if (nextPath) {
                this.makePath(nextPath[0], nextPath[1]);
            }
        }

        // Если есть текущий маршрут и в нем еще есть элементы 
        if (this.path && this.path.length) {
            if (process.env.VERBOSE_MOB_INFO == 1 ) console.log("cells in path remains: " + this.path.length);
            if (process.env.VERBOSE_MOB_INFO == 1 ) console.log("this.x " + this.position.x + " this.y " + this.position.y)
            // Следующая точка в маршруте – первая
            var nextCell = this.path[0];

            // Если текущая позиция совпадают с текущей точкой
            if (
                this.position.x > nextCell[1] - 0.1 && this.position.x < nextCell[1] + 0.1 &&
                this.position.z > nextCell[0] - 0.1 && this.position.z < nextCell[0] + 0.1
                ) {

                this.position.x = nextCell[1];
                this.position.z = nextCell[0];
                // Удалить первую точку из маршрута
                this.path.shift();
                nextCell = this.path[0];
            } 

            if (nextCell) {
            // Если текущая позиция не совпадают со следующей точкой
                // Сделать вектор из текущей позиции
                var curPos = new Vector3(this.position.x, this.position.y, this.position.z);
                // Взять следующую точку
                // Певернуть координаты, т.к. поиск пути возвращает другие оси
                var nextVector = new Vector3(nextCell[1], this.position.y, nextCell[0]);

                // Вычислить текущую скорость 
                this.velocity = nextVector.sub(curPos);
                // this.position = nextVector;
                // this.position.x = nextVector.x;
                // this.position.z = nextVector.z;

                // console.log("Bot stays at " +  curVector.x + " " +  curVector.y + " " +  curVector.z);
                // console.log("Bot moving to " + nextVector.x + " " + nextVector.y + " " + nextVector.z);
                // console.log("Bot velocity is " + this.velocity.x + " " + this.velocity.y + " " + this.velocity.z);
            }
                
            // }

            // if (this.velocity.x != 0)
            //     this.position.x = nextVector.x + (this.speed * delta);

            // if (this.velocity.z != 0)
            //     this.position.z = this.position.z + (this.speed * delta);
            //     // this.position.z = nextVector.z + (this.speed * delta);

            // this.velocity = new Vector3(0,0,0);

            // console.log('path cells remains: ' + this.path.length);

        }
    	

        // console.log(this.position.x);

    }


}