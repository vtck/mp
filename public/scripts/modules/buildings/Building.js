var path    = require("path");
var fs = require('fs');

class Building {
    
    getChunks(arr, size) {
        return [].concat.apply([],
            arr.map(function(elem,i) {
                var ret = (arr.slice(i,i+size)).toString().split(",");
                for (var rr = ret.length - 1; rr >= 0; rr--) {
                    ret[rr] = ret[rr].split("");
                }
                return i%size ? [] : ret;
            })
        );
    } 
    getBaseMatrix () {

        const testFolder = __dirname + "/" + this.constructor.name + '/base/';
        const fs = require('fs');
        var files = [];

        fs.readdirSync(testFolder).forEach(file => {
            if (file.endsWith(".map.js"))
                files.push(file);
        })

        var filename = files[Math.floor(Math.random()*files.length)];

        return this.getMatrix(testFolder + filename)

    }
    getFloorMatrix () {


        const testFolder = __dirname + "/" + this.constructor.name  + '/floors/';
        const fs = require('fs');
        var files = [];

        fs.readdirSync(testFolder).forEach(file => {
            if (file.endsWith(".map.js"))
                files.push(file);
        })

        var filename = files[Math.floor(Math.random()*files.length)];

        return this.getMatrix(testFolder + filename)
        
    }
    getMatrix (filename) {

        var data = require('fs').readFileSync(filename, 'utf8').toString().split('\n\n');
        var levels = data.length;

        var matrix = new Array(levels);
        for (var layerIndex = 0; layerIndex < data.length; layerIndex++) {
            var rows = data[layerIndex].split("\n");
            var arr = [];
            for (var rowIndex = 0; rowIndex < rows.length; rowIndex++) {
                var cells = rows[rowIndex].split("");
                arr.push([]);
                arr[rowIndex].push(new Array(cells.length));
                for (var cellIndex = 0; cellIndex < cells.length; cellIndex++) {
                    var value = cells[cellIndex];
                    if ( value && this.dict.hasOwnProperty(value)) {
                        arr[rowIndex][cellIndex] = this.dict[value];
                    } else {
                        arr[rowIndex][cellIndex] = 0;   
                    }
                }
            }
            matrix[layerIndex] = arr;


        }
        return matrix;
    }
}

module.exports = Building;