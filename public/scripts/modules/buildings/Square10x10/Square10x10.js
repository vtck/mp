const Building = require('./../Building.js')

class Square10x10 extends Building {
    constructor () {
    	super();
        this.dict = {
        	"1" : 32,
            "2" : 321,
            "5" : 321,
            "W" : 80,
            "L" : 401
        }
    }

}

module.exports = Square10x10