const Building = require('./../Building.js')

class Square8x8 extends Building {
    constructor () {
    	super();
        this.dict = {
        	"1" : 32,
        	"2" : 321,
        	"*" : 22,
        	"D" : 60,
        	"E" : 70,
            "G" : 80
        }
    }

}

module.exports = Square8x8