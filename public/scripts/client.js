
var worldMatrix, worldEntities;
var worldLightmap;
var currentPlayer = {
    id : null
};

var worldPeers = [];
var worldUpdateState = [];

var testGlow = null;


var globalShadows = false;
var doCulling = true;

var cachedChunks = [];
var loadedChunks = [];
var loadChunk;
var unloadChunkByName;
var unloadChunk;
var createPlanes;
var preloadChunks;
var setChunksVisibility;
var doors = [];
var boxes = [];
var windows = [];

var chunksGroup;
var chunkSize = 20;
var renderDistance = 2;


var playerModel = null;
var worldPlayers = {};

var globalMultiplier = 1;

THREE.Cache.enabled = false;

var blocker = document.getElementById( 'blocker' );
var instructions = document.getElementById( 'instructions' );

var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;
if ( havePointerLock ) {
    var element = document.body;
    var pointerlockchange = function ( event ) {
        if ( document.pointerLockElement === element || document.mozPointerLockElement === element || document.webkitPointerLockElement === element ) {
            controlsEnabled = true;
            controls.enabled = true;
            blocker.style.display = 'none';
        } else {
            controls.enabled = false;
            blocker.style.display = 'block';
            instructions.style.display = '';
        }
    };
    var pointerlockerror = function ( event ) {
        instructions.style.display = '';
    };
    // Hook pointer lock state change events
    document.addEventListener( 'pointerlockchange', pointerlockchange, false );
    document.addEventListener( 'mozpointerlockchange', pointerlockchange, false );
    document.addEventListener( 'webkitpointerlockchange', pointerlockchange, false );
    document.addEventListener( 'pointerlockerror', pointerlockerror, false );
    document.addEventListener( 'mozpointerlockerror', pointerlockerror, false );
    document.addEventListener( 'webkitpointerlockerror', pointerlockerror, false );
    instructions.addEventListener( 'click', function ( event ) {
        instructions.style.display = 'none';
        // Ask the browser to lock the pointer
        element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;
        element.requestPointerLock();
    }, false );
} else {
    instructions.innerHTML = 'Your browser doesn\'t seem to support Pointer Lock API';
}

var controlsEnabled = false;
var moveForward = false;
var moveBackward = false;
var moveLeft = false;
var moveRight = false;
var canJump = false;
var velocity = new THREE.Vector3();
var direction = new THREE.Vector3();

// var controls = new THREE.OrbitControls( camera, renderer.domElement );
// var controls = new THREE.FirstPersonControls( camera );
// controls.movementSpeed = 10;
// controls.lookSpeed = 0.125;
// controls.lookVertical = true;


var keyboard = new THREEx.KeyboardState();

var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer( { antialias: true } );

if ( globalShadows ) {
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;     
}

renderer.setPixelRatio( window.devicePixelRatio );
renderer.setSize( window.innerWidth, window.innerHeight );

document.body.appendChild( renderer.domElement );

window.addEventListener( 'resize', onWindowResize, false );

var stats = new Stats();
stats.showPanel( 0 );
document.body.appendChild( stats.dom );

var scene = new THREE.Scene();
scene.name = "world";
window.scene = scene;
window.THREE = THREE;

var controls = new THREE.PointerLockControls( camera );
scene.add( controls.getObject() );

// scene.fog = new THREE.Fog(0xbbbbbb, 2, 1000);

var mixer, animationClip;

// LIGHTS
// hemiLight = new THREE.HemisphereLight( 0xffffff, 0x444444, 1 );
// hemiLight.position.set( 0, 50, 0 );
// scene.add( hemiLight );
// hemiLightHelper = new THREE.HemisphereLightHelper( hemiLight, 10 );
// scene.add( hemiLightHelper );
//
// dirLight = new THREE.DirectionalLight( 0xffffff, 1 );
// dirLight.color.setHSL( 0.1, 1, 0.95 );
// dirLight.position.set( -1, 1.75, 1 );
// dirLight.position.multiplyScalar( 30 );
// scene.add( dirLight );

// if ( globalShadows ) {

//     dirLight.castShadow = true;
//     dirLight.shadow.mapSize.width = 2048;
//     dirLight.shadow.mapSize.height = 2048;
//     var d = 100;
//     dirLight.shadow.camera.left = -d;
//     dirLight.shadow.camera.right = d;
//     dirLight.shadow.camera.top = d;
//     dirLight.shadow.camera.bottom = -d;
//     dirLight.shadow.camera.far = 1000;
//     dirLight.shadow.bias = -0.0001;

// }

// dirLightHeper = new THREE.DirectionalLightHelper( dirLight, 10 );
// scene.add( dirLightHeper );

// var randomLightCount = 10;
// var maxLightX = 20;
// var maxLightZ = 20;
// var maxLightY = 20;

// for (var i = 0; i < randomLightCount; i++) {
//     var tempX = Math.floor(Math.random() * maxLightX) + 1;
//     var tempZ = Math.floor(Math.random() * maxLightZ) + 1;
//     var tempY = Math.floor(Math.random() * maxLightY) + 3;

//     var light = new THREE.PointLight( 0xffffff, 1, 25 );
//     light.position.set( tempX, tempY, tempZ );
//     scene.add( light );
    
// }





// var light = new THREE.DirectionalLight( 0xffffff, 1 );
// light.position.set( 0, 1, 0    );          //default; light shining from top
// light.shadow.mapSize.width = 512;  // default
// light.shadow.mapSize.height = 512; // default
// light.shadow.camera.near = 0.5;    // default
// light.shadow.camera.far = 500;     // default
// light.castShadow = true;            // default false
// scene.add( light );

//Set up shadow properties for the light


// camera.position.x = 8;
// camera.position.y = 10;
// camera.position.z = 8;

// camera.lookAt(new THREE.Vector3(12,5,12));


// var cameraPosition = new THREE.Vector3(0,0,0);
// var cameraPositionTween = new TWEEN.Tween(cameraPosition);

// function moveCamera(tx,ty,tz) {

//     cameraPosition.x = camera.position.x
//     cameraPosition.y = camera.position.y
//     cameraPosition.z = camera.position.z

//     cameraPositionTween.stop().to({
//         x: tx,
//         y: ty,
//         z: tz
//     }, 1000)
//     .onUpdate(function() {
//         camera.position.x = cameraPosition.x;
//         camera.position.y = cameraPosition.y;
//         camera.position.z = cameraPosition.z;
//     })
//     .easing(TWEEN.Easing.Quadratic.Out)
//     .start();
// }

// var particleCount = 1800,
//     particles = new THREE.Geometry(),
//     pMaterial = new THREE.ParticleBasicMaterial({
//       color: 0xFFFFFF,
//       size: 20
//     });

// // now create the individual particles
// for (var p = 0; p < particleCount; p++) {

//   // create a particle with random
//   // position values, -250 -> 250
//   var pX = Math.random() * 500 - 250,
//       pY = Math.random() * 500 - 250,
//       pZ = Math.random() * 500 - 250,
//       particle = new THREE.Vertex(
//         new THREE.Vector3(pX, pY, pZ)
//       );

//   // add it to the geometry
//   particles.vertices.push(particle);
// }

// // create the particle system
// var particleSystem = new THREE.ParticleSystem(
//     particles,
//     pMaterial);

// // add it to the scene
// scene.add(particleSystem);

var clock = new THREE.Clock();


var onProgress = function ( xhr ) {
    if ( xhr.lengthComputable ) {
        var percentComplete = xhr.loaded / xhr.total * 100;
        console.log( Math.round(percentComplete, 2) + '% downloaded' );
        document.getElementById("gameStatus").innerHTML = Math.round(percentComplete, 2) + '% downloaded';
    }
};
var onError = function ( xhr ) { };

// THREE.Loader.Handlers.add( /\.dds$/i, new THREE.DDSLoader() );
// var mtlLoader = new THREE.MTLLoader();
// mtlLoader.setPath( 'models/steve/' );
// mtlLoader.load( 'bryan.mtl', function( materials ) {
//     materials.preload();
//     var objLoader = new THREE.OBJLoader();
//     objLoader.setMaterials( materials );
//     objLoader.setPath( 'models/steve/' );
//     objLoader.load( 'bryan.obj', function ( obj ) {
//         playerModel = obj;
//         window.addEventListener("load", init, false);

//     }, onProgress, onError );
// });

var steveModel;
new THREE.ObjectLoader().load( 'models/steve/minecraft-steve2.json', function ( obj ) {
    steveModel = obj;
    steveModel.name = "model";
    var head = steveModel.getObjectByName("head")
    head.material.side = 0;
    steveModel.rotation.y = - Math.PI / 2;
    steveModel.position.y = -0.6;
    init();
})

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

function init() {

    document.getElementById("gameStatus").innerHTML = "Loading started";

    // lensflares
    var textureLoader = new THREE.TextureLoader();

    // var textureFlare0 = textureLoader.load( 'textures/lensflare0.png' );
    // var textureFlare3 = textureLoader.load( 'textures/lensflare3.png' );
    var cellingTexture = textureLoader.load( 'textures/celling.png' );

    var midtownTexture = textureLoader.load(Globals.TILES.WALL.MIDTOWN.TEXTURE)
    midtownTexture.wrapS = midtownTexture.wrapT = THREE.RepeatWrapping;

    var indoorTexture = textureLoader.load(Globals.TILES.WALL.MIDTOWN_ALT.TEXTURE)
    indoorTexture.wrapS = indoorTexture.wrapT = THREE.RepeatWrapping;

    var groundTexture = textureLoader.load(Globals.TILES.GROUND.DEFAULT.TEXTURE)
    indoorTexture.wrapS = indoorTexture.wrapT = THREE.RepeatWrapping;
    // groundTexture.wrapS = THREE.RepeatWrapping;
        // groundTexture.wrapT = THREE.RepeatWrapping;
        // groundTexture.repeat.set( mapX, mapZ );

    // let glowMaterial = new THREE.ShaderMaterial({
    //     uniforms: {
    //       viewVector: {
    //         type: "v3",
    //         value: camera.position
    //       }
    //     },
    //     vertexShader: document.getElementById('vertexShaderSun').textContent,
    //     fragmentShader: document.getElementById('fragmentShaderSun').textContent,
    //     side: THREE.DoubleSide,
    //     blending: THREE.AdditiveBlending,
    //     transparent: true
    // });


    var socket = io.connect('http://localhost:3000/');

    socket.on("loggedIn", socketLoggedIn);
    socket.on("loadMapMatrix", socketLoadMapMatrix);
    socket.on("serverTick", socketServerTick);
    socket.on("updateMatrix", socketUpdateMatrix);

    function socketLoggedIn (data) {
        currentPlayer.id = data.player_id;
        currentPlayer.velocity = new THREE.Vector3(0,0,0);
        currentPlayer.rotation = new THREE.Vector3(0,0,0);
        console.log ("Connected as " + currentPlayer.id);
        document.getElementById("gameStatus").innerHTML = "Connected as " + currentPlayer.id;
    }

    function loadModels (doors, boxes, windows) {

        // new THREE.ObjectLoader().load( 'models/door-threejs/door.json', function ( obj ) {
            
        //     for(var i = 0; i < 1; i++) {
        //         var door = doors[i];

        //         obj.position.set( door.x, door.y + 0.5, door.z);
        //         obj.rotation.y = 0;
        //         obj.rotateY(door.r);
        //         obj.updateMatrix();
        //         obj.name = "door-x" + door.x + "y" + door.y + "z" + door.z;


        //         mixer = new THREE.AnimationMixer(obj);
        //         animationClip = obj.animations[ 0 ];
        //         animationClip.speed = 100;
        //         mixer.clipAction( animationClip ).play();
                

        //         scene.add(obj);

        THREE.Loader.Handlers.add( /\.dds$/i, new THREE.DDSLoader() );
        var mtlLoader = new THREE.MTLLoader();
        
        mtlLoader.setPath( 'models/box-obj2/' );
        mtlLoader.load( 'box.mtl', function( materials ) {
            materials.preload();
            var objLoader = new THREE.OBJLoader();
            objLoader.setMaterials( materials );
            objLoader.setPath( 'models/box-obj2/' );
            objLoader.load( 'box.obj', function ( boxObj ) {

                for(var i = 0; i < boxes.length; i++) {
                    var box = boxes[i];
                    boxObj.position.set( box.x, box.y, box.z);
                    boxObj.updateMatrix();
                    boxObj.name = "box";
                    scene.add(boxObj.clone());
                }

            }, onProgress, onError );
        });


        mtlLoader.setPath( 'models/door-obj/' );
        mtlLoader.load( 'door.mtl', function( materials ) {
            materials.preload();
            var objLoader = new THREE.OBJLoader();
            objLoader.setMaterials( materials );
            objLoader.setPath( 'models/door-obj/' );
            objLoader.load( 'door.obj', function ( obj ) {

                for(var i = 0; i < doors.length; i++) {
                    var door = doors[i];
                    obj.position.set( door.x, door.y + 0.5, door.z);
                    obj.rotation.y = 0;
                    obj.rotateY(door.r);
                    obj.updateMatrix();
                    obj.name = "door";

                    var clonedObj = obj.clone();

                    // mixer = new THREE.AnimationMixer(clonedObj);
                    // animationClip = clonedObj.animations[ 0 ];
                    // animationClip.speed = 100;
                    // mixer.clipAction( animationClip ).play();

                    scene.add(clonedObj);
                }

            }, onProgress, onError );
        });


        mtlLoader.setPath( 'models/window-obj/' );
        mtlLoader.load( 'window.mtl', function( materials ) {
            materials.preload();
            var objLoader = new THREE.OBJLoader();
            objLoader.setMaterials( materials );
            objLoader.setPath( 'models/window-obj/' );
            objLoader.load( 'window.obj', function ( obj ) {

                for(var i = 0; i < windows.length; i++) {
                    var w_model = windows[i];
                    obj.position.set( w_model.x, w_model.y, w_model.z);
                    obj.rotation.y = 0;
                    obj.rotateY(w_model.r);
                    obj.updateMatrix();
                    obj.name = "window";
                    var clonedObj = obj.clone();

                    // mixer = new THREE.AnimationMixer(clonedObj);
                    // animationClip = clonedObj.animations[ 0 ];
                    // animationClip.speed = 100;
                    // mixer.clipAction( animationClip ).play();

                    scene.add(clonedObj);
                }

            }, onProgress, onError );
        });


    }

    

    var exludedByPlanes = [];
    var wallsPlanes = [];
    
    function getBlock(x, y, z) {
        if (y in worldMatrix)
            if (x in worldMatrix[y])
                if (z in worldMatrix[y][x])
                    return worldMatrix[y][x][z]
        return -1;
    }

    function getLightLevel(x, y, z) {
        if (y in worldLightmap)
            if (x in worldLightmap[y])
                if (z in worldLightmap[y][x])
                    return worldLightmap[y][x][z]
        return -1;
    }

    function getBlockFace(x, y, z, face) {
        if (y in worldMatrix)
            if (x in worldMatrix[y])
                if (z in worldMatrix[y][x]) {
                    
                    var value = worldMatrix[y][x][z];
                    
                    if (face == "south") {
                        if ((x-1) in worldMatrix[y]) {
                            return worldMatrix[y][x-1][z];
                        }
                    }

                    if (face == "north") {
                        if ((x+1) in worldMatrix[y])
                            return worldMatrix[y][x+1][z];
                    }

                    if (face == "top") {
                        if ((y+1) in worldMatrix)
                            return worldMatrix[y+1][x][z];
                    }

                    if (face == "bottom") {
                        if ((y-1) in worldMatrix)
                            return worldMatrix[y-1][x][z];
                    }

                    if (face == "west") {
                        if ((z-1) in worldMatrix[y][x])
                            return worldMatrix[y][x][z-1];
                    }

                    if (face == "east") {
                        if ((z+1) in worldMatrix[y][x])
                            return worldMatrix[y][x][z+1];
                    }

                }
        return -1;
    }

    function getLightmapFace(x, y, z, face) {
        if (y in worldLightmap)
            if (x in worldLightmap[y])
                if (z in worldLightmap[y][x]) {
                    
                    var value = worldLightmap[y][x][z];
                    
                    if (face == "south") {
                        if ((x-1) in worldLightmap[y]) {
                            return worldLightmap[y][x-1][z];
                        }
                    }

                    if (face == "north") {
                        if ((x+1) in worldLightmap[y])
                            return worldLightmap[y][x+1][z];
                    }

                    if (face == "top") {
                        if ((y+1) in worldLightmap)
                            return worldLightmap[y+1][x][z];
                    }

                    if (face == "bottom") {
                        if ((y-1) in worldLightmap)
                            return worldLightmap[y-1][x][z];
                    }

                    if (face == "west") {
                        if ((z-1) in worldLightmap[y][x])
                            return worldLightmap[y][x][z-1];
                    }

                    if (face == "east") {
                        if ((z+1) in worldLightmap[y][x])
                            return worldLightmap[y][x][z+1];
                    }

                }
        return 0;
    }

    function setBlock(x, y, z, value) {
        if (y in worldMatrix)
            if (x in worldMatrix[y])
                if (z in worldMatrix[y][x])
                    worldMatrix[y][x][z] = value;
    }

   
  


    function isSolidBlock(val){
        var solids = [2, 32, 321, -1];
        if (solids.includes(val)) return true;
        else return false;
    }

    preloadChunks = function () {

        document.getElementById("gameStatus").innerHTML = "Map preloading started";

        chunksGroup = new THREE.Group();

        var lights = [];

        var mapX = worldMatrix[0].length;
        var mapZ = worldMatrix[0][0].length;

        for (chunkX = 0; chunkX < Math.ceil(mapX / chunkSize); chunkX++) {
            for (chunkZ = 0; chunkZ < Math.ceil(mapZ / chunkSize); chunkZ++) {

                let chunkName = "x" + chunkX + "z" + chunkZ;
                // console.log("loading chunk " + chunkName);
                document.getElementById("gameStatus").innerHTML = "Loading chunk " + chunkName;
                // loadedChunks.push(chunkName);
                                
                var chunkGeometry = new THREE.Geometry();
                
                var chunkMaterials = [];
                var geometries = [];
                // var lightmaps = [];

                for (var yI = 0; yI < worldMatrix.length; yI++) {
                    var layer = worldMatrix[yI];
                    for (var xI = (chunkX > 0 ? chunkX * chunkSize : chunkX); xI < (chunkX > 0 ? chunkX * chunkSize : chunkX) + chunkSize; xI++) {
                        var xRow = layer[xI];
                        for (var zI = (chunkZ > 0 ? chunkZ * chunkSize : chunkZ); zI < (chunkZ > 0 ? chunkZ * chunkSize : chunkZ) + chunkSize; zI++) {
                            
                            var x = xI;
                            var y = yI;
                            var z = zI;

                            var value = getBlock(x,y,z);
                            
                            // Если это стена, то проверяем каждый фейс
                            var faces = ["north", "south", "top", "bottom", "west", "east"];
                            faces.reverse();

                            if (isSolidBlock(value)) {

                                for (var i = faces.length - 1; i >= 0; i--) {

                                    var lightLevel = getLightmapFace(x,y,z, faces[i]);

                                    if (!geometries[value]){
                                        geometries[value] = [];
                                    }

                                    if(!geometries[value][lightLevel]){
                                        geometries[value][lightLevel] = new THREE.Geometry();
                                    }


                                    createPlanes(x, y, z, value, faces[i], [0, 80, 401], geometries[value][lightLevel]);
                                }
                            }



                           // if (value == 60 && getBlock(x, y+1, z) == 60) {
                           //      doors.push({
                           //          x : x,
                           //          y : y,
                           //          z : z,
                           //          r : (getBlock(x+1,y,z) === 0) ? Math.PI / 2 : 0
                           //      })
                           // }

                           if (value == 70) {
                                boxes.push({
                                    x : x,
                                    y : y,
                                    z : z,
                                    r : (getBlock(x+1,y,z) === 0) ? Math.PI / 2 : 0
                                })
                           }

                           if (value == 80) {
                                windows.push({
                                    x : x,
                                    y : y,
                                    z : z,
                                    r : (getBlock(x+1,y,z) === 0) ? Math.PI / 2 : 0
                                })
                            }

                            if (value == 401) {


                                // let light = new THREE.PointLight(0xffffff, ); // white light
                                //  light.position.set(x, y - 1, z);
                                //  scene.add(light);
                                
                                var lPlane = new THREE.Mesh(new THREE.PlaneGeometry(1,1,1), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: .9, side: THREE.DoubleSide }));
                                lPlane.name = "lamp plane";
                                lPlane.position.x = x;
                                lPlane.position.y = y + 0.499;
                                // lPlane.position.y = y;
                                lPlane.position.z = z;
                                lPlane.rotateX(- Math.PI / 2);
                                lPlane.updateMatrix();
                                  scene.add(lPlane);

                                  // if(!testGlow) 
                                    // testGlow = lPlane;



                                    // var lensflare = new THREE.Lensflare();
                                    // lensflare.addElement( new THREE.LensflareElement( textureFlare0, 10, 0, new THREE.Color(0xffffff) ) );
                                    // // lensflare.addElement( new THREE.LensflareElement( textureFlare0, 200, 0.05, new THREE.Color(0xffffff) ) );
                                    // // lensflare.addElement( new THREE.LensflareElement( textureFlare3, 60, 0.6 ) );
                                    // // lensflare.addElement( new THREE.LensflareElement( textureFlare3, 70, 0.7 ) );
                                    // // lensflare.addElement( new THREE.LensflareElement( textureFlare3, 120, 0.9 ) );
                                    // // lensflare.addElement( new THREE.LensflareElement( textureFlare3, 70, 1 ) );
                                    
                                    // lPlane.add(lensflare);


                                // addLight( 0.08, 0.8, 0.5,    0, 0, - 1000 );
                                // addLight( 0.995, 0.5, 0.9, 5000, 5000, - 1000 );

                                                

                                
                                // var glowMesh    = new THREEx.GeometricGlowMesh(lPlane);
                                
                                // var insideUniforms  = glowMesh.insideMesh.material.uniforms
                                // insideUniforms.coeficient.value    = 1.1
                                // insideUniforms.power.value     = 1.4
                                // insideUniforms.glowColor.value.set( 0xff69b4 ); 
                                
                                // var outsideUniforms = glowMesh.outsideMesh.material.uniforms
                                // outsideUniforms.glowColor.value.set('hotpink');


                                // lPlane.add(glowMesh.object3d);


                                // testGlow = new THREE.Mesh( lPlane.geometry, customMaterial.clone() );
                                // testGlow.name = "glow effect";
                                // testGlow.position.x = x;
                                // testGlow.position.y = y;
                                // testGlow.position.z = z;
                                // testGlow.scale.multiplyScalar(1.5);
                                // testGlow.material.uniforms[ "p" ].value = 2.7;
                                // testGlow.material.uniforms[ "c" ].value = 0;
                                // testGlow.material.uniforms.glowColor.value.setHex("0xffffff"); 
                                // testGlow.material.side = THREE.FrontSide;  
                                // // testGlow.material.side = THREE.BackSide;
                                // // testGlow.material.blending = THREE.NormalBlending;  
                                // testGlow.material.blending = THREE.AdditiveBlending; 

                                // scene.add( testGlow );
                            }
                            

                        }
                    }
                }

                // console.log('-----------------')
                // console.log(geometries);

                geometries.forEach(function(geo, geoindex) {
                    
                    geo.forEach(function(lightLevel, levelIndex) {

                        // var color = 0x787878;
                        var color = 0x222222;

                        switch (levelIndex) {
                            case 1:
                                color = 0x333333;
                                break;
                            case 2:
                                color = 0x555555;
                                break;
                            case 3:
                                color = 0x888888;
                                break;
                            case 4:
                                color = 0xbbbbbb;
                                break;
                            case 5:
                                color = 0xdddddd;
                                break;
                            case 6:
                                color = 0xeeeeee;
                                break;
                            case 7:
                                color = 0xffffff;
                                break;
                        }

                        if (geoindex == 32) {
                            var geoMat = new THREE.MeshBasicMaterial( { map : midtownTexture , wireframe : false, color : color } );
                        } else if (geoindex == 321) {
                            var geoMat = new THREE.MeshBasicMaterial( { map : indoorTexture , wireframe : false, color : color } );
                        } else if (geoindex == 2) {
                            var geoMat = new THREE.MeshBasicMaterial( { map : groundTexture , wireframe : false, color : color } );
                        } else if (geoindex == 401) {
                            var geoMat = new THREE.MeshNormalMaterial( { color : 0xffffff , wireframe : false } );
                        }


                        var geoMesh = new THREE.Mesh(lightLevel, geoMat);
                        scene.add(geoMesh);   
                    })

                    
                    // chunkGeometry.merge(geoMesh.geometry, geoMesh.matrix);
                })


                // lightmaps.forEach(function(geo, lightindex) {
                //     // if (lightindex == 32) {
                //     //     var lightMat = new THREE.MeshPhongMaterial( { map : midtownTexture, wireframe : false } );
                //     //     // chunkMaterials.push(new THREE.MeshPhongMaterial( { wireframe : true, color: 0xff0000 } ))
                //     // } else if (lightindex == 321) {
                //     //     // var geoMat = new THREE.MeshPhongMaterial( { wireframe : true, color: 0xff0000 } );
                //     //     var lightMat = new THREE.MeshPhongMaterial( { map : indoorTexture, wireframe : false } );
                //     // } else {
                //     //     var lightMat = new THREE.MeshNormalMaterial( { color : 0xffffff, wireframe : false } );
                //     // }
                //     var lightMat = new THREE.MeshBasicMaterial( { color : 0xffffff, wireframe : false, side: THREE.DoubleSide } );
                //     var lightMesh = new THREE.Mesh(geo, lightMat);
                //     lightMesh.name = "lightMesh";
                //     scene.add(lightMesh);
                //     // chunkGeometry.merge(geoMesh.geometry, geoMesh.matrix);
                // })

                // scene.add(chunkMesh);
                // chunksGroup.add(chunkMesh);


            }
        }

        // loadModels(doors, boxes, windows);



        scene.add(chunksGroup);

        if (false)
        for (var yI = 0; yI < worldLightmap.length; yI++) {
            for (var xI = 0; xI < worldLightmap[yI].length; xI++) {
                for (var zI = 0; zI < worldLightmap[yI][xI].length; zI++) {
                    var x = xI;
                    var y = yI;
                    var z = zI;

                    var lightLevel = worldLightmap[yI][xI][zI];

                    if (lightLevel > 0) {

                        var opacity = 0;

                        switch (lightLevel) {
                            case 1:
                                opacity = .2;
                                break;
                            case 2:
                                opacity = .3;
                                break;
                            case 3:
                                opacity = .4;
                                break;
                            case 4:
                                opacity = .5;
                                break;
                            case 5:
                                opacity = .6;
                                break;
                            case 6:
                                opacity = .7;
                                break;
                            case 7:
                                opacity = .8;
                                break;
                        }

                        var lightBlock = new THREE.Mesh(new THREE.BoxGeometry(1,1,1), new THREE.MeshBasicMaterial( { color : 0xffffff, transparent: true, opacity : opacity } ));
                        lightBlock.position.x = x;
                        lightBlock.position.y = y;
                        lightBlock.position.z = z;
                        scene.add(lightBlock);

                    }
                }
            }
        }


        document.getElementById("gameStatus").innerHTML = "Click to play!";


        // MeshBasicMaterial({map: this.textures.n,  transparent:true});

    
        // var spriteMap = new THREE.TextureLoader().load( "textures/hitler.png" );
        // var spriteMaterial = new THREE.SpriteMaterial( { map: spriteMap, color: 0xffffff } );
        // var sprite = new THREE.Sprite( spriteMaterial );
        // sprite.position.set(13,1,20);
        // scene.add( sprite );       

        // for (var i = 0; i < lights.length; i++) {

        //     var light_el = lights[i];
            
            
        //     // var light = new THREE.PointLight( 0xffffff, 1, 25 );
        //     // light.position.set( light_el.x, light_el.y + 1, light_el.z );
        //     // scene.add( light );

        //     // Расчитаем освещенные блоки вниз
        //     console.log("свет исходит из блока " + light_el.x + " " + light_el.y + " " + light_el.z);

        //     var maxPower = 3;
        //     var faces = ["north", "south", "top", "bottom", "west", "east"];

        //     for (var p = -maxPower; p <= maxPower; p++) {

        //         if (!isSolidBlock(getBlock(light_el.x, (light_el.y + p), light_el.z))) {
                    
        //             console.log("блок " + light_el.x + " " + (light_el.y + p) + " " + light_el.z + " не твердый");

        //             // Проверим всех соседей этого блока
        //             for (var sibX = -1; sibX <= 1; sibX++) {
        //                 // По горизонтали
        //                 if (sibX != 0) {
        //                     if(isSolidBlock(getBlock(light_el.x + sibX, (light_el.y + p), light_el.z))) {

        //                         var opacityLevel = ((maxPower - Math.abs(p)) / maxPower) / 3;

        //                         var blend = new THREE.Mesh(new THREE.PlaneGeometry(1, 1), new THREE.MeshNormalMaterial({ side: THREE.DoubleSide, opacity: opacityLevel, transparent: true }));
        //                         blend.position.x = light_el.x + 0.499;
        //                         blend.position.y = light_el.y + p;
        //                         blend.position.z = light_el.z;
        //                         blend.rotateY(- Math.PI / 2);
        //                         blend.updateMatrix();
        //                         blend.name = "lightblend";

        //                         scene.add(blend);

        //                     }
        //                 }
                        
        //             }

        //             for (var sibY = -1; sibX <= 1; sibY++) {
        //                 // По вертикали
        //                 if(sibY !== 0) {
        //                     if(isSolidBlock(getBlock(light_el.x, (light_el.y + p) + sibY, light_el.z))) {

        //                         var opacityLevel = ((maxPower - Math.abs(p)) / maxPower) / 3;

        //                         var blend = new THREE.Mesh(new THREE.PlaneGeometry(1, 1), new THREE.MeshNormalMaterial({ side: THREE.DoubleSide, opacity: opacityLevel, transparent: true }));
        //                         blend.position.x = light_el.x;
        //                         blend.position.y = light_el.y + p + sibY + 0.5;
        //                         blend.position.z = light_el.z;
        //                         // blend.rotateZ(- Math.PI / 2);
        //                         blend.updateMatrix();
        //                         blend.name = "lightblendVert";

        //                         scene.add(blend);

        //                     }
        //                 }

        //             }

        //             // for (var sibZ = -1; sibZ <= 1; sibZ++) {
        //             //     // По глубине
        //             //     if(sibZ !== 0) {
                            
        //             //     }

        //             // }

                    

        //         }


        //         // Проверка готовых фейсов
        //         // if( excludedFaces.find( function(el) {
        //         //     return el.x == x && el.y == y && el.z == z && el.face == face
        //         // }))
        //         //     return;

        //         // if (getBlock(x, y, z) == value &&  !shouldTouchBlock.includes(getBlockFace(x, y, z, face)) )
        //         //     return;
        //     }

        // }

    }

    setChunksVisibility = function (x, z) {
        // var ob = scene.getObjectByName("x" + x + "z" + z);
        // if (ob)
            // ob.visible = false;
    }

    unloadChunkByName = function (chunkName) {
        var index = loadedChunks.indexOf(chunkName);
        if (index > -1) {
            loadedChunks.splice(index, 1);
            var chunkMesh = scene.getObjectByName(chunkName);
            if (chunkMesh) scene.remove( chunkMesh );
        }

    }
    

    unloadChunk = function (chunkX, chunkZ) {
        let chunkName = "x" + chunkX + "z" + chunkZ;
        unloadChunkByName(chunkName);
    }


    var excludedFaces = [];


    createPlanes = function (x, y, z, value, face, shouldTouchBlock = [], geometry) {

        document.getElementById("gameStatus").innerHTML = "Creating geometry..";

        var planeWidth = 1;
        var planeHeight = 1;
        var planeLength = 1;

        var planeX = x;
        var planeY = y;
        var planeZ = z;

        var currentLightLevel = getLightmapFace(x, y, z, face);

        if( excludedFaces.find( function(el) {
            return el.x == x && el.y == y && el.z == z && el.face == face
        }))
            return;

         if (getBlock(x, y, z) == value &&  !shouldTouchBlock.includes(getBlockFace(x, y, z, face)) )
            return;

        if (doCulling)
        for (var checkBlock = 1; checkBlock < 99; checkBlock++) {
            if (getBlock(x, y, z + checkBlock) == value && shouldTouchBlock.includes(getBlockFace(x, y, z + checkBlock, face)) ) {

                if( excludedFaces.find( function(el) {
                    return el.x == x && el.y == y && el.z == z + checkBlock && el.face == face
                }))
                    break;

                if (currentLightLevel !== getLightmapFace(x, y, z + checkBlock, face) ) {
                        break;
                }

                planeWidth++;
                planeZ += 0.5;

                excludedFaces.push({
                    x : x,
                    y : y,
                    z : z + checkBlock,
                    face : face
                })
            } else {
                break;
            }
        }

        if (doCulling)
        if (planeWidth === 1) {
            for (var checkBlock = 1; checkBlock < 99; checkBlock++) {
                
                if (getBlock(x + checkBlock, y, z) == value && getBlockFace(x + checkBlock, y, z, face) === 0 ) {

                    if( excludedFaces.find( function(el) {
                        return el.x == x + checkBlock && el.y == y && el.z == z && el.face == face
                    }))
                        break;

                    if (currentLightLevel !== getLightmapFace(x + checkBlock, y, z, face) ) {
                        break;
                    }

                    if (face == "south" || face == "north" || face == "west" || face == "east") {
                        planeWidth++;
                    } else {
                        planeHeight++;
                    }

                    planeX += 0.5;
                    
                    excludedFaces.push({
                        x : x + checkBlock,
                        y : y,
                        z : z,
                        face : face
                    })
                } else {
                    break;
                }
            }  
        }

        if (doCulling)
        if (planeWidth === 1 && planeHeight == 1) {
            for (var checkBlock = 1; checkBlock < 99; checkBlock++) {
                
                if (getBlock(x, y + checkBlock, z) == value && getBlockFace(x, y + checkBlock, z, face) === 0 ) {

                    if( excludedFaces.find( function(el) {
                        return el.x == x && el.y == y + checkBlock && el.z == z && el.face == face
                    }))
                        break;

                    if (currentLightLevel !== getLightmapFace(x, y + checkBlock, z, face) ) {
                        break;
                    }

                    if (face == "south" || face == "north" || face == "west" || face == "east") {
                        planeHeight++;
                        planeY += 0.5;
                    } else {
                        // planeLength++;
                        // planeX += 0.5;
                    }
                    
                    excludedFaces.push({
                        x : x,
                        y : y + checkBlock,
                        z : z,
                        face : face
                    })
                } else {
                    break;
                }
            }  
        }

        var plane = new THREE.Mesh(new THREE.PlaneGeometry(planeWidth, planeHeight));

        var uvs = plane.geometry.faceVertexUvs[ 0 ];
        uvs[ 0 ][ 0 ].set( 0, planeHeight );
        uvs[ 0 ][ 1 ].set( 0, 0 );
        uvs[ 0 ][ 2 ].set( planeWidth, planeHeight );
        uvs[ 1 ][ 0 ].set( 0, 0 );
        uvs[ 1 ][ 1 ].set( planeWidth, 0 );
        uvs[ 1 ][ 2 ].set( planeWidth, planeHeight );

        // console.log(plane)


        if (face == "south") {

            plane.position.x = planeX - 0.5;
            plane.position.y = planeY;
            plane.position.z = planeZ;
            plane.rotateY( - Math.PI / 2 );

        }

        if (face == "north") {
            plane.position.x = planeX + 0.5;
            plane.position.y = planeY;
            plane.position.z = planeZ;
            plane.rotateY( Math.PI / 2 );
        }

        if (face == "top") {
            plane.position.x = planeX;
            plane.position.y = planeY + 0.5;
            plane.position.z = planeZ;
            plane.rotateX( - Math.PI / 2);
            plane.rotateZ( Math.PI / 2);
        }

        if (face == "bottom") {
            plane.position.x = planeX;
            plane.position.y = planeY - 0.5;
            plane.position.z = planeZ;
            plane.rotateX( Math.PI / 2);
            plane.rotateZ( Math.PI / 2);
        }

        if (face == "west") {
            plane.position.x = planeX;
            plane.position.y = planeY;
            plane.position.z = planeZ - 0.5;
            plane.rotateX(- Math.PI);
            // plane.rotateZ(- Math.PI);
        }

        if (face == "east") {
            plane.position.x = planeX;
            plane.position.y = planeY;
            plane.position.z = planeZ + 0.5;
            plane.rotateY(- Math.PI);
            plane.rotateX(Math.PI);

        }

        plane.updateMatrix();
        geometry.merge(plane.geometry, plane.matrix);

        return geometry;
    }

    
    function socketLoadMapMatrix (data) {

        document.getElementById("gameStatus").innerHTML = "Data received from server";

        var boxes = [];

        worldMatrix = data.matrix;
        worldLightmap = data.lightmap;

        var mapX = worldMatrix[0].length;
        var mapZ = worldMatrix[0][0].length;

        // SKYBOX
        var skyMaterials = [
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load( "textures/skybox/posz-alt.png" ), side: THREE.DoubleSide }), //front side
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load( 'textures/skybox/negz-alt.png' ), side: THREE.DoubleSide }), //back side
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load( 'textures/skybox/posy-alt.png' ), side: THREE.DoubleSide }), //up side
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load( 'textures/skybox/negy-alt.png' ), side: THREE.DoubleSide }), //down side
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load( 'textures/skybox/negx-alt.png' ), side: THREE.DoubleSide }), //right side
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load( 'textures/skybox/posx-alt.png' ), side: THREE.DoubleSide }) //left side
        ];
        var skybox = new THREE.Mesh( new THREE.BoxGeometry( 1100, 1100, 1100 ), new THREE.MeshFaceMaterial( skyMaterials ) );
        skybox.name = "skybox";
        scene.add( skybox );

        preloadChunks();

        // loadChunk(0,0);
        // loadChunk(1,1);
        // loadChunk(2,2);


        // indoorMidtownWalls.updateMatrix();
        // // worldGeometry.merge(indoorMidtownWalls.geometry, indoorMidtownWalls.matrix, 1);

        // indoorMidtownFloors.updateMatrix();
        // // worldGeometry.merge(indoorMidtownFloors.geometry, indoorMidtownFloors.matrix, 2);

      
        // var axesHelper = new THREE.AxesHelper( 5 );
        // scene.add( axesHelper );

        // var glassTexture = new  THREE.TextureLoader().load(Globals.TILES.ENTITIES.GLASS.TEXTURE);
        // var glassMaterial = new THREE.MeshLambertMaterial({ map: glassTexture, transparent: true });
        // glassMaterial.opacity = 0.75;
        // var glassCube = new THREE.Mesh(new THREE.BoxGeometry(1,1,0.8), glassMaterial);
        // glassCube.position.x = 11;
        // glassCube.position.y = 1;
        // glassCube.position.z = 10;
        // scene.add(glassCube);

        // var xAxisCube = new THREE.Mesh(new THREE.BoxGeometry(1,1,1), new THREE.MeshPhongMaterial({ color: 0xff0000, wireframe: true }));
        // xAxisCube.position.x = 0;
        // xAxisCube.position.y = 0;
        // xAxisCube.position.z = 0;
        // scene.add(xAxisCube);
       

        // var scuba = new THREE.Mesh(new THREE.BoxGeometry(1,0.6,0.1), new THREE.MeshPhongMaterial( { map: new THREE.TextureLoader().load("textures/scuba.png"), side: THREE.DoubleSide } ));
        // scuba.castShadow = true;
        // scuba.receiveShadow = true;
        // scuba.position.x = 9;
        // scuba.position.z = 10;
        // scuba.position.y = 3;
        // scene.add(scuba);

        // var neon = new THREE.Mesh(new THREE.BoxGeometry(1,0.6,0.1), new THREE.MeshPhongMaterial( { map: new THREE.TextureLoader().load("textures/neon2.png"), side: THREE.DoubleSide } ));
        // neon.castShadow = true;
        // neon.receiveShadow = true;
        // neon.position.x = 9;
        // neon.position.z = 10;
        // neon.position.y = 3.7;
        // scene.add(neon);

    }

    function socketUpdateMatrix (data) {
        // console.log(data);
    }

    var animateInterval = 1000 / 90;

    var getInput = function () {


            if (keyboard.pressed("W")) {
                currentPlayer.velocity.x = 1;
            }

            if (keyboard.pressed("S")) {
                currentPlayer.velocity.x = -1;
            }

            if (keyboard.pressed("D")) {
                currentPlayer.velocity.z = 1;
            }

            if (keyboard.pressed("A")) {
                currentPlayer.velocity.z = -1;
            }

            if (keyboard.pressed("space")) {
                currentPlayer.velocity.y = 1;
            }

            let rotation = controls.getObject().rotation;

            currentPlayer.rotation = {
                x : rotation.x,
                y : rotation.y,
                z : rotation.z
            }

            let pitch = controls.getPitchObject().rotation.x;
            currentPlayer.pitch = pitch;

            sendState(currentPlayer);
    }

    var animate = function () {

        setTimeout( function() {

            stats.begin();

            var delta = clock.getDelta();
            
            TWEEN.update(delta);
            if (mixer) mixer.update( delta );

            physicsDelta = 100 * (parseFloat(parseFloat(100) / (parseFloat(1) / parseFloat(delta))))


            // if (testGlow) { 
            //     let viewVector = new THREE.Vector3().subVectors( camera.position, testGlow.glow.getWorldPosition());
            //     testGlow.glow.material.uniforms.viewVector.value = viewVector;
            // }
            

            // if (testGlow)
            //     testGlow.material.uniforms.viewVector.value = new THREE.Vector3().subVectors( camera.position, testGlow.position );

            getInput();

            for (peerid in worldPeers) {

                var peer = worldPeers[peerid];


                if ( peer.player.id == currentPlayer.id ) {

                    controls.getObject().position.x = THREE.Math.lerp(controls.getObject().position.x, peer.player.position.x,0.15);
                    controls.getObject().position.y = THREE.Math.lerp(controls.getObject().position.y, peer.player.position.y + 0.8,0.15);
                    controls.getObject().position.z = THREE.Math.lerp(controls.getObject().position.z, peer.player.position.z,0.15);
                
                } else {


                    var view = new THREE.Vector3();
                    
                    view.subVectors(controls.getObject().position, peer.mesh.position);
                    
                    var inverse = peer.mesh.quaternion.clone();
                    inverse.inverse();
                 
                    view.applyQuaternion(inverse);
                    
                    var angle = Math.round(Math.atan2(view.x, view.z) * 4/Math.PI);
                    var animation_speed = 1000 * delta;

                    if(angle === 0 || angle === -0){// North
                        peer.billboard.material.map = peer.textures.n;
                        peer.billboard.needsUpdate = true;
                        if (!peer.isStanding) {
                            peer.animations.n.update(animation_speed);    
                        } else {
                            // peer.animations.
                        }
                        
                    }
                    else if(angle === -1){// Northwest
                        peer.billboard.material.map = peer.textures.nw;
                        peer.billboard.needsUpdate = true;
                        if (!peer.isStanding) {
                            peer.animations.nw.update(animation_speed);    
                        } else {
                            // peer.animations.
                        }
                        
                    }
                    else if(angle === -2){// West
                        peer.billboard.material.map = peer.textures.w;
                        peer.billboard.needsUpdate = true;
                        if (!peer.isStanding) {
                            peer.animations.w.update(animation_speed);    
                        } else {
                            // peer.animations.
                        }
                        
                    }
                    else if(angle === -3){// Southwest
                        peer.billboard.material.map = peer.textures.sw;
                        peer.billboard.needsUpdate = true;
                        if (!peer.isStanding) {
                            peer.animations.sw.update(animation_speed);    
                        } else {
                            // peer.animations.
                        }
                        
                    }
                    else if(angle === 4 || angle === -4){// South
                        peer.billboard.material.map = peer.textures.s;
                        peer.billboard.needsUpdate = true;
                        if (!peer.isStanding) {
                            peer.animations.s.update(animation_speed);    
                        } else {
                            // peer.animations.
                        }
                        
                    }
                    else if(angle === 3){// Southeast
                        peer.billboard.material.map = peer.textures.se;
                        peer.billboard.needsUpdate = true;
                        if (!peer.isStanding) {
                            peer.animations.se.update(animation_speed);    
                        } else {
                            // peer.animations.
                        }
                        
                    }
                    else if(angle === 2){// East
                        peer.billboard.material.map = peer.textures.e;
                        peer.billboard.needsUpdate = true;
                        if (!peer.isStanding) {
                            peer.animations.e.update(animation_speed);    
                        } else {
                            // peer.animations.
                        }
                        
                    }
                    else if(angle === 1){// Northeast
                        peer.billboard.material.map = peer.textures.ne;
                        peer.billboard.needsUpdate = true;
                        if (!peer.isStanding) {
                            peer.animations.ne.update(animation_speed);    
                        } else {
                            // peer.animations.
                        }
                        
                    }

                    var lookWhere = controls.getObject().position.clone();
                    lookWhere.y = peer.billboard.position.y;
                    peer.billboard.lookAt(lookWhere);

                }

                peer.updatePosition();




            }

            renderer.render(scene, camera);
            requestAnimationFrame( animate );
            
            stats.end();

        }, animateInterval );

    };

    animate();


    // Тик от сервера

    function socketServerTick (data) {
        
        document.getElementById("netstat").innerHTML = "Online: " + data.roomPlayers.length;

        // Список всех игроков в игре
        // worldPlayers = data.roomPlayers;

        // Пройтись по списку онлайн игроков
        // for (var i = 0; i < worldPlayers.length; i++) {

        //     var player = worldPlayers[i];

        //     // Найти объект игрока на сцене
        //     var ob = scene.getObjectByName("Player" + player.id);

        //     if (!ob) {

                    

        //             var objGeo = new THREE.Geometry();
        //             // objGeo.merge(new THREE.BoxGeometry(player.xlen, player.ylen, player.zlen));
                    
        //             var obj = new THREE.Mesh(
        //                 objGeo,
        //                 new THREE.MeshPhongMaterial({ color: 0xff0000, wireframe: true })
        //             );

        //             var dir = new THREE.Vector3( 0, 0, -1 );
        //             //normalize the direction vector (convert to vector of length 1)
        //             dir.normalize();
        //             var origin = new THREE.Vector3( 0, 0.7, 0 );
        //             var length = 0.7;
        //             var hex = 0xffff00;

        //             var arrowHelper = new THREE.ArrowHelper( dir, origin, length, hex );
        //             arrowHelper.name = "arrow";
        //             obj.add( arrowHelper );

        //             // obj.add(steveModel.clone());

        //             obj.userData.name = player.id;
        //             obj.name = "Player" + player.id;
        //             obj.position.set( player.position.x, player.position.y, player.position.z);

        //             if (currentPlayer.id == player.id) {
        //                 obj.visible = false;
        //             }

        //             scene.add(obj);

        //             worldPeers[player.id] = new Peer(scene, player.id, player.position.x, player.position.y, player.position.z);

        //     }

        // }
        

        for (var i = 0; i < data.roomPlayers.length; i++) {

            var player = data.roomPlayers[i];

            if(worldPeers[player.id]) {

                worldPeers[player.id].player = player;

            } else {

                worldPeers[player.id] = new Peer(scene, player);

            }


            
            
        }

        

    }

    function sendState(player) {
 
        try {

            var html = "";

            // html += player.velocity.x + " " + player.velocity.y + " " + player.velocity.z;
            // var rotation = controls.getObject().rotation;
            
            // html += "<br>Camera " + JSON.stringify(controls.getObject().rotation);

            html += JSON.stringify(renderer.info.render);

            document.getElementById("logstat").innerHTML = html;


        } catch (e) {

        }


        socket.emit("clientSendState", {
            "id"       : player.id,
            "velocity" : player.velocity,
            "rotation" : player.rotation,
            "pitch"    : player.pitch
        });

        currentPlayer.velocity = { x: 0, y: 0, z: 0};
    }

    function activateObject(x, y, z){
        socket.emit("clientActivateObject", {
            "id"       : currentPlayer.id,
            "obj"      : {
                x: x,
                y: y,
                z: z
            }
        });
    }

}

var Peer = function(ob, player) {

    this.player = player;

    this.textures = {
        s:new THREE.ImageUtils.loadTexture('textures/player_0/s.png'),
        se:new THREE.ImageUtils.loadTexture('textures/player_0/se.png'),
        e:new THREE.ImageUtils.loadTexture('textures/player_0/e.png'),
        ne:new THREE.ImageUtils.loadTexture('textures/player_0/ne.png'),
        n:new THREE.ImageUtils.loadTexture('textures/player_0/n.png'),
        nw:new THREE.ImageUtils.loadTexture('textures/player_0/nw.png'),
        w:new THREE.ImageUtils.loadTexture('textures/player_0/w.png'),
        sw:new THREE.ImageUtils.loadTexture('textures/player_0/sw.png')
    };

    this.animations = {
        s:new TextureAnimator(this.textures.s, 4, 1, 4, 160),
        se:new TextureAnimator(this.textures.se, 4, 1, 4, 160),
        e:new TextureAnimator(this.textures.e, 4, 1, 4, 160),
        ne:new TextureAnimator(this.textures.ne, 4, 1, 4, 160),
        n:new TextureAnimator(this.textures.n, 4, 1, 4, 160),
        nw:new TextureAnimator(this.textures.nw, 4, 1, 4, 160),
        w:new TextureAnimator(this.textures.w, 4, 1, 4, 160),
        sw:new TextureAnimator(this.textures.sw, 4, 1, 4, 160)
    };

    this.billboard_mat = new THREE.MeshBasicMaterial({map: this.textures.n,  transparent:true});
    this.billboard_geom = new THREE.PlaneGeometry(2, 2, 2, 1);
    this.billboard = new THREE.Mesh(this.billboard_geom, this.billboard_mat);
    this.billboard.name = this.player.id + '_billboard';
    this.billboard.rotation.set(0,0,0);

    if (this.player.id == currentPlayer.id){
        this.billboard.visible = false;
    }
    
    this.material = new THREE.MeshBasicMaterial({transparent:true, opacity:0});
    this.geometry = new THREE.PlaneGeometry(0.1, 0.1, 0.1, 1);
    this.mesh = new THREE.Mesh(this.geometry, this.material);
    this.mesh.name = this.player.id + '_mesh';
    this.mesh.visible = false;
    this.mesh.rotation.set(0,0,0);

    this.billboard.position.set(this.player.position.x,this.player.position.y,this.player.position.z);
    this.mesh.position.set(this.player.position.x,this.player.position.y,this.player.position.z);

    // this.mesh.updateMatrix();
    // this.billboard.updateMatrix();


    var dir = new THREE.Vector3( 0, 0, -1 );
    dir.normalize(); //normalize the direction vector (convert to vector of length 1)
    var origin = new THREE.Vector3( 0, 0.7, 0 );
    var length = 0.7;
    var hex = 0xffff00;

    var arrowHelper = new THREE.ArrowHelper( dir, origin, length, hex );
    arrowHelper.name = "arrow";
    arrowHelper.visible = false;
    this.mesh.add( arrowHelper );

    
    // var obj = new THREE.Mesh(
    //     new THREE.BoxGeometry(player.xlen, player.ylen, player.zlen),
    //     new THREE.MeshBasicMaterial({ color: 0xff0000, wireframe: true })
    // );

    // this.mesh.add(obj);


    scene.add(this.billboard);
    scene.add(this.mesh);
    // scene.add(this.box);


    
    this.direction = '';

};


Peer.prototype.updatePosition = function(){

    var lerpPower = .15;
    
    this.mesh.position.x = THREE.Math.lerp(this.mesh.position.x, this.player.position.x, lerpPower);
    this.mesh.position.y = THREE.Math.lerp(this.mesh.position.y, this.player.position.y, lerpPower);
    this.mesh.position.z = THREE.Math.lerp(this.mesh.position.z, this.player.position.z, lerpPower);

    this.billboard.position.x = this.mesh.position.x;
    this.billboard.position.y = this.mesh.position.y + 0.2;
    this.billboard.position.z = this.mesh.position.z;

    // this.billboard.position.x = this.player.position.x;
    // this.billboard.position.y = this.player.position.y + 0.2;
    // this.billboard.position.z = this.player.position.z;
    
    // this.mesh.position.x = this.player.position.x;
    // this.mesh.position.y = this.player.position.y;
    // this.mesh.position.z = this.player.position.z;

    this.mesh.rotation.x = this.player.rotation.x;
    this.mesh.rotation.y = this.player.rotation.y;
    this.mesh.rotation.z = this.player.rotation.z;

    var ar = this.mesh.getObjectByName("arrow");
    ar.rotation.x = this.player.pitch - (Math.PI / 2);

    // this.mesh.quaternion.set(netObj.quat.x, netObj.quat.y, netObj.quat.z, netObj.quat.w);
    // this.mesh.quaternion.normalize();
    
};


// var scale = 1;
// var mouseX = 0;
// var mouseY = 0;

// camera.rotation.order = "XYZ"; // this is not the default

// document.addEventListener( "mousemove", mouseMove, false );

// function mouseMove( event ) {

//     mouseX = - ( event.clientX / renderer.domElement.clientWidth ) * 2 + 1;
//     mouseY = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;

//     camera.rotation.x = mouseY / scale;
//     camera.rotation.y = mouseX / scale;

// }
