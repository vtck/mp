
var worldMatrix, worldEntities;
var currentPlayer = {
    id : null
};


var cachedChunks = [];
var loadedChunks = [];
var loadChunk;
var unloadChunkByName;
var unloadChunk;
var createPlanes;
var preloadChunks;

var chunkSize = 24;
var renderDistance = 1;


var playerModel = null;
var worldPlayers = {};

var globalMultiplier = 1;

THREE.Cache.enabled = false;

var blocker = document.getElementById( 'blocker' );
var instructions = document.getElementById( 'instructions' );

var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;
if ( havePointerLock ) {
    var element = document.body;
    var pointerlockchange = function ( event ) {
        if ( document.pointerLockElement === element || document.mozPointerLockElement === element || document.webkitPointerLockElement === element ) {
            controlsEnabled = true;
            controls.enabled = true;
            blocker.style.display = 'none';
        } else {
            controls.enabled = false;
            blocker.style.display = 'block';
            instructions.style.display = '';
        }
    };
    var pointerlockerror = function ( event ) {
        instructions.style.display = '';
    };
    // Hook pointer lock state change events
    document.addEventListener( 'pointerlockchange', pointerlockchange, false );
    document.addEventListener( 'mozpointerlockchange', pointerlockchange, false );
    document.addEventListener( 'webkitpointerlockchange', pointerlockchange, false );
    document.addEventListener( 'pointerlockerror', pointerlockerror, false );
    document.addEventListener( 'mozpointerlockerror', pointerlockerror, false );
    document.addEventListener( 'webkitpointerlockerror', pointerlockerror, false );
    instructions.addEventListener( 'click', function ( event ) {
        instructions.style.display = 'none';
        // Ask the browser to lock the pointer
        element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;
        element.requestPointerLock();
    }, false );
} else {
    instructions.innerHTML = 'Your browser doesn\'t seem to support Pointer Lock API';
}

var controlsEnabled = false;
var moveForward = false;
var moveBackward = false;
var moveLeft = false;
var moveRight = false;
var canJump = false;
var velocity = new THREE.Vector3();
var direction = new THREE.Vector3();

// var controls = new THREE.OrbitControls( camera, renderer.domElement );
// var controls = new THREE.FirstPersonControls( camera );
// controls.movementSpeed = 10;
// controls.lookSpeed = 0.125;
// controls.lookVertical = true;


var keyboard = new THREEx.KeyboardState();

var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer( { antialias: true } );
renderer.setPixelRatio( window.devicePixelRatio );
renderer.setSize( window.innerWidth, window.innerHeight );

document.body.appendChild( renderer.domElement );

window.addEventListener( 'resize', onWindowResize, false );

var stats = new Stats();
stats.showPanel( 0 );
document.body.appendChild( stats.dom );

var scene = new THREE.Scene();
scene.name = "world";
window.scene = scene;
window.THREE = THREE;


var controls = new THREE.PointerLockControls( camera );
scene.add( controls.getObject() );


// scene.fog = new THREE.Fog(0xbbbbbb, 2, 1000);


var mixer, animationClip;

// LIGHTS
hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, .6 );
hemiLight.color.setHSL( 0.6, 1, 0.6 );
hemiLight.groundColor.setHSL( 0.095, 1, 0.75 );
hemiLight.position.set( 0, 50, 0 );
scene.add( hemiLight );
hemiLightHelper = new THREE.HemisphereLightHelper( hemiLight, 10 );
scene.add( hemiLightHelper );
//
dirLight = new THREE.DirectionalLight( 0xffffff, 1 );
dirLight.color.setHSL( 0.1, 1, 0.95 );
dirLight.position.set( -1, 1.75, 1 );
dirLight.position.multiplyScalar( 30 );
scene.add( dirLight );
dirLight.castShadow = true;
dirLight.shadow.mapSize.width = 2048;
dirLight.shadow.mapSize.height = 2048;
var d = 100;
dirLight.shadow.camera.left = -d;
dirLight.shadow.camera.right = d;
dirLight.shadow.camera.top = d;
dirLight.shadow.camera.bottom = -d;
dirLight.shadow.camera.far = 1000;
dirLight.shadow.bias = -0.0001;
dirLightHeper = new THREE.DirectionalLightHelper( dirLight, 10 );
scene.add( dirLightHeper );


// var light = new THREE.PointLight( 0xff0000, 1, 100 );
// light.position.set( 50, 0, 50 );
// scene.add( light );


// var light = new THREE.DirectionalLight( 0xffffff, 1 );
// light.position.set( 0, 1, 0    );          //default; light shining from top
// light.shadow.mapSize.width = 512;  // default
// light.shadow.mapSize.height = 512; // default
// light.shadow.camera.near = 0.5;    // default
// light.shadow.camera.far = 500;     // default
// light.castShadow = true;            // default false
// scene.add( light );

//Set up shadow properties for the light


// camera.position.x = 8;
// camera.position.y = 10;
// camera.position.z = 8;

// camera.lookAt(new THREE.Vector3(12,5,12));


// var animatedRotation = 0;


function tweenRotation(ob,to) {

    return (function (ob, to) {
        
        try {

            var animatedRotation = ob.rotation.y;
            var rotationTween = new TWEEN.Tween(animatedRotation);
            rotationTween.stop().to(to, 100)
            .onUpdate(function() {
                ob.rotation.y = animatedRotation;
                ob.updateMatrix();
            })
            .easing(TWEEN.Easing.Quadratic.Out)
            .start();

        } catch (e) {

        }

        
    })();

}


var cameraPosition = new THREE.Vector3(0,0,0);
var cameraPositionTween = new TWEEN.Tween(cameraPosition);

function moveCamera(tx,ty,tz) {

    cameraPosition.x = camera.position.x
    cameraPosition.y = camera.position.y
    cameraPosition.z = camera.position.z

    cameraPositionTween.stop().to({
        x: tx,
        y: ty,
        z: tz
    }, 1000)
    .onUpdate(function() {
        camera.position.x = cameraPosition.x;
        camera.position.y = cameraPosition.y;
        camera.position.z = cameraPosition.z;
    })
    .easing(TWEEN.Easing.Quadratic.Out)
    .start();
}

// var cameraRotation = new THREE.Vector3(1,0,0);
// var cameraRotationTween = new TWEEN.Tween(cameraRotation);

function rotateCamera(tr) {

    cameraRotation.y = camera.rotation.y

    cameraRotationTween.stop().to({
        y: tr
    }, 1000)
    .onUpdate(function() {
        camera.rotation.y = cameraRotation.y;
    })
    .easing(TWEEN.Easing.Quadratic.Out)
    .start();
}

// var particleCount = 1800,
//     particles = new THREE.Geometry(),
//     pMaterial = new THREE.ParticleBasicMaterial({
//       color: 0xFFFFFF,
//       size: 20
//     });

// // now create the individual particles
// for (var p = 0; p < particleCount; p++) {

//   // create a particle with random
//   // position values, -250 -> 250
//   var pX = Math.random() * 500 - 250,
//       pY = Math.random() * 500 - 250,
//       pZ = Math.random() * 500 - 250,
//       particle = new THREE.Vertex(
//         new THREE.Vector3(pX, pY, pZ)
//       );

//   // add it to the geometry
//   particles.vertices.push(particle);
// }

// // create the particle system
// var particleSystem = new THREE.ParticleSystem(
//     particles,
//     pMaterial);

// // add it to the scene
// scene.add(particleSystem);

var clock = new THREE.Clock();


var onProgress = function ( xhr ) {
    if ( xhr.lengthComputable ) {
        var percentComplete = xhr.loaded / xhr.total * 100;
        console.log( Math.round(percentComplete, 2) + '% downloaded' );
    }
};
var onError = function ( xhr ) { };

// THREE.Loader.Handlers.add( /\.dds$/i, new THREE.DDSLoader() );
// var mtlLoader = new THREE.MTLLoader();
// mtlLoader.setPath( 'models/steve/' );
// mtlLoader.load( 'bryan.mtl', function( materials ) {
//     materials.preload();
//     var objLoader = new THREE.OBJLoader();
//     objLoader.setMaterials( materials );
//     objLoader.setPath( 'models/steve/' );
//     objLoader.load( 'bryan.obj', function ( obj ) {
//         playerModel = obj;
//         window.addEventListener("load", init, false);

//     }, onProgress, onError );
// });

 window.addEventListener("load", init, false);


 function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}


function init() {


    var socket = io.connect('http://localhost:3000/');

    socket.on("loggedIn", socketLoggedIn);
    socket.on("loadMapMatrix", socketLoadMapMatrix);
    socket.on("serverTick", socketServerTick);
    socket.on("updateMatrix", socketUpdateMatrix);

    function socketLoggedIn (data) {
        currentPlayer.id = data.player_id;
        currentPlayer.velocity = new THREE.Vector3(0,0,0);
        console.log ("Connected as " + currentPlayer.id);
    }


    function loadDoors (doors) {

        // new THREE.ObjectLoader().load( 'models/door-threejs/door.json', function ( obj ) {
            
        //     for(var i = 0; i < 1; i++) {
        //         var door = doors[i];

        //         obj.position.set( door.x, door.y + 0.5, door.z);
        //         obj.rotation.y = 0;
        //         obj.rotateY(door.r);
        //         obj.updateMatrix();
        //         obj.name = "door-x" + door.x + "y" + door.y + "z" + door.z;


        //         mixer = new THREE.AnimationMixer(obj);
        //         animationClip = obj.animations[ 0 ];
        //         animationClip.speed = 100;
        //         mixer.clipAction( animationClip ).play();
                

        //         scene.add(obj);




        //     }
        // });

        // var onEnd = function ( xhr ) {
        // }

        // var onProgress = function ( xhr ) {
        //     if ( xhr.lengthComputable ) {
        //         var percentComplete = xhr.loaded / xhr.total * 100;
        //         console.log( Math.round(percentComplete, 2) + '% downloaded' );
        //     }
        // };
        // var onError = function ( xhr ) { };

        // THREE.Loader.Handlers.add( /\.dds$/i, new THREE.DDSLoader() );
        // var mtlLoader = new THREE.MTLLoader();
        // mtlLoader.setPath( 'models/door-obj/' );
        // mtlLoader.load( 'door.mtl', function( materials ) {
        //     materials.preload();
        //     var objLoader = new THREE.OBJLoader();
        //     objLoader.setMaterials( materials );
        //     objLoader.setPath( 'models/door-obj/' );
        //     objLoader.load( 'door.obj', function ( obj ) {

        //         var doorsGeo = new THREE.Geometry();

        //         for(var i = 0; i < doors.length; i++) {
        //             var door = doors[i];
        //             obj.position.set( door.x, door.y + 0.5, door.z);
        //             obj.rotation.y = 0;
        //             obj.rotateY(door.r);
        //             obj.updateMatrix();
        //             obj.name = "door";
        //             scene.add(obj.clone());
        //         }

        //     }, onProgress, onError );
        // });

    }

    

    var exludedByPlanes = [];
    var wallsPlanes = [];
    
    function getBlock(x, y, z) {
        if (y in worldMatrix)
            if (x in worldMatrix[y])
                if (z in worldMatrix[y][x])
                    return worldMatrix[y][x][z]
        return -1;
    }

    function getBlockFace(x, y, z, face) {
        if (y in worldMatrix)
            if (x in worldMatrix[y])
                if (z in worldMatrix[y][x]) {
                    
                    var value = worldMatrix[y][x][z];
                    
                    if (face == "south") {
                        if ((x-1) in worldMatrix[y]) {
                            return worldMatrix[y][x-1][z];
                        }
                    }

                    if (face == "north") {
                        if ((x+1) in worldMatrix[y])
                            return worldMatrix[y][x+1][z];
                    }

                    if (face == "top") {
                        if ((y+1) in worldMatrix)
                            return worldMatrix[y+1][x][z];
                    }

                    if (face == "bottom") {
                        if ((y-1) in worldMatrix)
                            return worldMatrix[y-1][x][z];
                    }

                    if (face == "west") {
                        if ((z-1) in worldMatrix[y][x])
                            return worldMatrix[y][x][z-1];
                    }

                    if (face == "east") {
                        if ((z+1) in worldMatrix[y][x])
                            return worldMatrix[y][x][z+1];
                    }

                }
        return -1;
    }

    function setBlock(x, y, z, value) {
        if (y in worldMatrix)
            if (x in worldMatrix[y])
                if (z in worldMatrix[y][x])
                    worldMatrix[y][x][z] = value;
    }

    var midtownTexture = new THREE.TextureLoader().load(Globals.TILES.WALL.MIDTOWN.TEXTURE)
    midtownTexture.wrapS = midtownTexture.wrapT = THREE.RepeatWrapping;

    var indoorTexture = new THREE.TextureLoader().load(Globals.TILES.INDOOR.MIDTOWN.WALL_TEXTURE)
    indoorTexture.wrapS = indoorTexture.wrapT = THREE.RepeatWrapping;
        
    var worldMaterials = [
        new THREE.MeshPhongMaterial( { map : midtownTexture , wireframe : false } ),
        new THREE.MeshPhongMaterial( { map : indoorTexture , wireframe : false } )
    ];

    preloadChunks = function () {

        var mapX = worldMatrix[0].length;
        var mapZ = worldMatrix[0][0].length;

        for (chunkX = 0; chunkX < Math.ceil(mapX / chunkSize); chunkX++) {
            for (chunkZ = 0; chunkZ < Math.ceil(mapZ / chunkSize); chunkZ++) {

                let chunkName = "x" + chunkX + "z" + chunkZ;
                console.log("loading chunk " + chunkName);
                
                var chunkGeometry = new THREE.Geometry();
                var wallsGeometry = new THREE.Geometry();
                var indoorGeometry = new THREE.Geometry();

                // loadedChunks.push(chunkName);

                for (var yI = 0; yI < worldMatrix.length; yI++) {
                    var layer = worldMatrix[yI];
                    for (var xI = (chunkX > 0 ? chunkX * chunkSize : chunkX); xI < (chunkX > 0 ? chunkX * chunkSize : chunkX) + chunkSize; xI++) {
                        var xRow = layer[xI];
                        for (var zI = (chunkZ > 0 ? chunkZ * chunkSize : chunkZ); zI < (chunkZ > 0 ? chunkZ * chunkSize : chunkZ) + chunkSize; zI++) {
                            
                            var x = xI;
                            var y = yI;
                            var z = zI;

                            var value = getBlock(x,y,z);

                            // Если это стена, то проверяем каждый фейс

                            if (value == 32 && getBlockFace(x, y, z, "north") === 0 ) {
                                createPlanes(x, y, z, value, "north", 0, wallsGeometry, true);
                            }

                            if (value == 32 && getBlockFace(x, y, z, "south") === 0 ) {
                                createPlanes(x, y, z, value, "south", 0, wallsGeometry, true);
                            }

                            if (value == 32 && getBlockFace(x, y, z, "top") === 0 ) {
                                createPlanes(x, y, z, value, "top", 0, wallsGeometry, true);
                            }

                            if (value == 32 && getBlockFace(x, y, z, "bottom") === 0 ) {
                                createPlanes(x, y, z, value, "bottom", 0, wallsGeometry, true);
                            }

                            if (value == 32 && getBlockFace(x, y, z, "west") === 0 ) {
                                createPlanes(x, y, z, value, "west", 0, wallsGeometry, true);
                            }

                            if (value == 32 && getBlockFace(x, y, z, "east") === 0 ) {
                                createPlanes(x, y, z, value, "east", 0, wallsGeometry, true);
                            }

                            // Внутреннее пространство


                           if (value == 22 && getBlockFace(x, y, z, "north") === 32 ) {
                               createPlanes(x, y, z, value, "north", 32, indoorGeometry, false);

                           }

                           if (value == 22 && getBlockFace(x, y, z, "south") === 32 ) {
                               createPlanes(x, y, z, value, "south", 32, indoorGeometry);
                           }

                           if (value == 22 && getBlockFace(x, y, z, "top") === 32 ) {
                               createPlanes(x, y, z, value, "top", 32, indoorGeometry, false);

                           }

                           if (value == 22 && getBlockFace(x, y, z, "bottom") === 32 ) {
                               createPlanes(x, y, z, value, "bottom", 32, indoorGeometry, false);
                           }


                           if (value == 22 && getBlockFace(x, y, z, "west") === 32 ) {
                               createPlanes(x, y, z, value, "west", 32, indoorGeometry, false);
                           }

                           if (value == 22 && getBlockFace(x, y, z, "east") === 32 ) {
                               createPlanes(x, y, z, value, "east", 32, indoorGeometry, false);
                           }

                           if (value == 60 && getBlock(x, y+1, z) == 60) {
                                // doors.push({
                                //     x : x,
                                //     y : y,
                                //     z : z,
                                //     r : Math.PI / 2
                                // })
                           }
                            

                        }
                    }
                }

                var wallsMesh = new THREE.Mesh(wallsGeometry);
                wallsMesh.updateMatrix();
                chunkGeometry.merge(wallsMesh.geometry, wallsMesh.matrix);

                var indoorMesh = new THREE.Mesh(indoorGeometry);
                indoorMesh.updateMatrix();
                chunkGeometry.merge(indoorMesh.geometry, indoorMesh.matrix, 1);

                var chunkMesh = new THREE.Mesh(chunkGeometry, worldMaterials);
                chunkMesh.name = chunkName;

                scene.add(chunkMesh);

            }
        }

    }


    unloadChunkByName = function (chunkName) {
        var index = loadedChunks.indexOf(chunkName);
        if (index > -1) {
            loadedChunks.splice(index, 1);
            var chunkMesh = scene.getObjectByName(chunkName);
            if (chunkMesh) scene.remove( chunkMesh );
        }

    }
    

    unloadChunk = function (chunkX, chunkZ) {
        let chunkName = "x" + chunkX + "z" + chunkZ;
        unloadChunkByName(chunkName);
    }

    // loadChunk = function (chunkX, chunkZ) {

    //     let chunkName = "x" + chunkX + "z" + chunkZ;
    //     console.log("loading chunk " + chunkName);

    //     if (!loadedChunks.includes(chunkName)) {

    //         var chunkGeometry = new THREE.Geometry();
    //         var wallsGeometry = new THREE.Geometry();
    //         var indoorGeometry = new THREE.Geometry();

    //         loadedChunks.push(chunkName);

    //         for (var yI = 0; yI < worldMatrix.length; yI++) {
    //             var layer = worldMatrix[yI];
    //             for (var xI = (chunkX > 0 ? chunkX * chunkSize : chunkX); xI < (chunkX > 0 ? chunkX * chunkSize : chunkX) + chunkSize; xI++) {
    //                 var xRow = layer[xI];
    //                 for (var zI = (chunkZ > 0 ? chunkZ * chunkSize : chunkZ); zI < (chunkZ > 0 ? chunkZ * chunkSize : chunkZ) + chunkSize; zI++) {
                        
    //                     var x = xI;
    //                     var y = yI;
    //                     var z = zI;

    //                     var value = getBlock(x,y,z);

    //                     // Если это стена, то проверяем каждый фейс

    //                     if (value == 32 && getBlockFace(x, y, z, "north") === 0 ) {
    //                         createPlanes(x, y, z, value, "north", 0, wallsGeometry, true);
    //                     }

    //                     if (value == 32 && getBlockFace(x, y, z, "south") === 0 ) {
    //                         createPlanes(x, y, z, value, "south", 0, wallsGeometry, true);
    //                     }

    //                     if (value == 32 && getBlockFace(x, y, z, "top") === 0 ) {
    //                         createPlanes(x, y, z, value, "top", 0, wallsGeometry, true);
    //                     }

    //                     if (value == 32 && getBlockFace(x, y, z, "bottom") === 0 ) {
    //                         createPlanes(x, y, z, value, "bottom", 0, wallsGeometry, true);
    //                     }

    //                     if (value == 32 && getBlockFace(x, y, z, "west") === 0 ) {
    //                         createPlanes(x, y, z, value, "west", 0, wallsGeometry, true);
    //                     }

    //                     if (value == 32 && getBlockFace(x, y, z, "east") === 0 ) {
    //                         createPlanes(x, y, z, value, "east", 0, wallsGeometry, true);
    //                     }

    //                     // Внутреннее пространство


    //                    if (value == 22 && getBlockFace(x, y, z, "north") === 32 ) {
    //                        createPlanes(x, y, z, value, "north", 32, indoorGeometry, false);

    //                    }

    //                    if (value == 22 && getBlockFace(x, y, z, "south") === 32 ) {
    //                        createPlanes(x, y, z, value, "south", 32, indoorGeometry);
    //                    }

    //                    if (value == 22 && getBlockFace(x, y, z, "top") === 32 ) {
    //                        createPlanes(x, y, z, value, "top", 32, indoorGeometry, false);

    //                    }

    //                    if (value == 22 && getBlockFace(x, y, z, "bottom") === 32 ) {
    //                        createPlanes(x, y, z, value, "bottom", 32, indoorGeometry, false);
    //                    }


    //                    if (value == 22 && getBlockFace(x, y, z, "west") === 32 ) {
    //                        createPlanes(x, y, z, value, "west", 32, indoorGeometry, false);
    //                    }

    //                    if (value == 22 && getBlockFace(x, y, z, "east") === 32 ) {
    //                        createPlanes(x, y, z, value, "east", 32, indoorGeometry, false);
    //                    }

    //                    if (value == 60 && getBlock(x, y+1, z) == 60) {
    //                         // doors.push({
    //                         //     x : x,
    //                         //     y : y,
    //                         //     z : z,
    //                         //     r : Math.PI / 2
    //                         // })
    //                    }
                        

    //                 }
    //             }
    //         }

    //         var midtownTexture = new THREE.TextureLoader().load(Globals.TILES.WALL.MIDTOWN.TEXTURE)
    //         midtownTexture.wrapS = midtownTexture.wrapT = THREE.RepeatWrapping;

    //         var indoorTexture = new THREE.TextureLoader().load(Globals.TILES.INDOOR.MIDTOWN.WALL_TEXTURE)
    //         indoorTexture.wrapS = indoorTexture.wrapT = THREE.RepeatWrapping;
                
    //         var worldMaterials = [
    //             new THREE.MeshPhongMaterial( { map : midtownTexture , wireframe : false } ),
    //             new THREE.MeshPhongMaterial( { map : indoorTexture , wireframe : false } ),
    //             // new THREE.MeshPhongMaterial( { color: 0xff0000, wireframe: true } ),
    //             // new THREE.MeshPhongMaterial( { color: 0x00ff00, wireframe: true } ),
    //             // new THREE.MeshPhongMaterial( { color: 0x0000ff, wireframe: true } )
    //         ];

    //         var wallsMesh = new THREE.Mesh(wallsGeometry);
    //         wallsMesh.updateMatrix();
    //         chunkGeometry.merge(wallsMesh.geometry, wallsMesh.matrix);

    //         var indoorMesh = new THREE.Mesh(indoorGeometry);
    //         indoorMesh.updateMatrix();
    //         chunkGeometry.merge(indoorMesh.geometry, indoorMesh.matrix, 1);

    //         var chunkMesh = new THREE.Mesh(chunkGeometry, worldMaterials);
    //         chunkMesh.name = chunkName;



    //         // scene.add(chunkMesh);

    //     }

        

    // }

    function updateChunks () {

    }


    var excludedFaces = [];


    createPlanes = function (x, y, z, value, face, shouldTouchBlock, geometry, isOuter) {

        var planeWidth = 1;
        var planeHeight = 1;

        var planeX = x;
        var planeY = y;
        var planeZ = z;

        if( excludedFaces.find( function(el) {
            return el.x == x && el.y == y && el.z == z && el.face == face
        }))
            return;

        for (var checkBlock = 1; checkBlock < 20; checkBlock++) {
            if (getBlock(x, y, z + checkBlock) == value && getBlockFace(x, y, z + checkBlock, face) === shouldTouchBlock ) {

                if( excludedFaces.find( function(el) {
                    return el.x == x && el.y == y && el.z == z + checkBlock && el.face == face
                }))
                    break;


                planeWidth++;
                planeZ += 0.5;
                excludedFaces.push({
                    x : x,
                    y : y,
                    z : z + checkBlock,
                    face : face
                })
            } else {
                break;
            }
        }

        if (planeWidth === 1) {
            for (var checkBlock = 1; checkBlock < 20; checkBlock++) {
                
                if (getBlock(x + checkBlock, y, z) == value && getBlockFace(x + checkBlock, y, z, face) === 0 ) {

                    if( excludedFaces.find( function(el) {
                        return el.x == x + checkBlock && el.y == y && el.z == z && el.face == face
                    }))
                        break;

                    if (face == "south" || face == "north" || face == "west" || face == "east") {
                        planeWidth++;
                        planeX += 0.5;
                    } else {
                        planeHeight++;
                        planeX += 0.5;
                    }
                    
                    excludedFaces.push({
                        x : x + checkBlock,
                        y : y,
                        z : z,
                        face : face
                    })
                } else {
                    break;
                }
            }  
        }

        var plane = new THREE.Mesh(new THREE.PlaneGeometry(planeWidth, planeHeight));

        var uvs = plane.geometry.faceVertexUvs[ 0 ];
        uvs[ 0 ][ 0 ].set( 0, planeHeight );
        uvs[ 0 ][ 1 ].set( 0, 0 );
        uvs[ 0 ][ 2 ].set( planeWidth, planeHeight );
        uvs[ 1 ][ 0 ].set( 0, 0 );
        uvs[ 1 ][ 1 ].set( planeWidth, 0 );
        uvs[ 1 ][ 2 ].set( planeWidth, planeHeight );

        if (isOuter) {

            if (face == "south") {

                plane.position.x = planeX - 0.5;
                plane.position.y = planeY;
                plane.position.z = planeZ;
                plane.rotateY( - Math.PI / 2 );

            }

            if (face == "north") {
                plane.position.x = planeX + 0.5;
                plane.position.y = planeY;
                plane.position.z = planeZ;
                plane.rotateY( Math.PI / 2 );
            }

            if (face == "top") {
                plane.position.x = planeX;
                plane.position.y = planeY + 0.5;
                plane.position.z = planeZ;
                plane.rotateX( - Math.PI / 2);
                plane.rotateZ( Math.PI / 2);
            }

            if (face == "bottom") {
                plane.position.x = planeX;
                plane.position.y = planeY - 0.5;
                plane.position.z = planeZ;
                plane.rotateX( Math.PI / 2);
                plane.rotateZ( Math.PI / 2);
            }

            if (face == "west") {
                plane.position.x = planeX;
                plane.position.y = planeY;
                plane.position.z = planeZ - 0.5;
                plane.rotateX(- Math.PI);
                // plane.rotateZ(- Math.PI);
            }

            if (face == "east") {
                plane.position.x = planeX;
                plane.position.y = planeY;
                plane.position.z = planeZ + 0.5;
                plane.rotateY(- Math.PI);
                plane.rotateX(Math.PI);

            }

        } else if (!isOuter) {


            if (face == "south") {

                plane.position.x = planeX - 0.5;
                plane.position.y = planeY;
                plane.position.z = planeZ;
                plane.rotateY( Math.PI / 2 );

            }

            if (face == "north") {
                plane.position.x = planeX + 0.5;
                plane.position.y = planeY;
                plane.position.z = planeZ;
                plane.rotateY( - Math.PI / 2 );
            }

            if (face == "top") {
                plane.position.x = planeX;
                plane.position.y = planeY + 0.5;
                plane.position.z = planeZ;
                plane.rotateX( Math.PI / 2);
                plane.rotateZ( - Math.PI / 2);
            }

            if (face == "bottom") {
                plane.position.x = planeX;
                plane.position.y = planeY - 0.5;
                plane.position.z = planeZ;
                plane.rotateX(- Math.PI / 2);
                plane.rotateZ(- Math.PI / 2);
            }

            if (face == "west") {
                plane.position.x = planeX;
                plane.position.y = planeY;
                plane.position.z = planeZ - 0.5;
                // plane.rotateX(- Math.PI);
                // plane.rotateZ( Math.PI);
            }

            if (face == "east") {
                plane.position.x = planeX;
                plane.position.y = planeY;
                plane.position.z = planeZ + 0.5;
                plane.rotateY(Math.PI);
                // plane.rotateX(Math.PI);

            }

        }

        plane.updateMatrix();
        geometry.merge(plane.geometry, plane.matrix);
    }

    
    function socketLoadMapMatrix (data) {

        var doors = [];
        var boxes = [];

        worldMatrix = data;

        var mapX = worldMatrix[0].length;
        var mapZ = worldMatrix[0][0].length;

        // SKYBOX
        var skyMaterials = [
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load( "textures/skybox/posz-alt.png" ), side: THREE.DoubleSide }), //front side
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load( 'textures/skybox/negz-alt.png' ), side: THREE.DoubleSide }), //back side
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load( 'textures/skybox/posy-alt.png' ), side: THREE.DoubleSide }), //up side
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load( 'textures/skybox/negy-alt.png' ), side: THREE.DoubleSide }), //down side
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load( 'textures/skybox/negx-alt.png' ), side: THREE.DoubleSide }), //right side
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load( 'textures/skybox/posx-alt.png' ), side: THREE.DoubleSide }) //left side
        ];
        var skybox = new THREE.Mesh( new THREE.BoxGeometry( 1100, 1100, 1100 ), new THREE.MeshFaceMaterial( skyMaterials ) );
        skybox.name = "skybox";
        scene.add( skybox );

        var groundTexture  = new THREE.TextureLoader().load(Globals.TILES.GROUND.DEFAULT.TEXTURE);
        groundTexture.wrapS = THREE.RepeatWrapping;
        groundTexture.wrapT = THREE.RepeatWrapping;
        groundTexture.repeat.set( mapX, mapZ );

        var groundMesh = new THREE.Mesh(new THREE.PlaneGeometry( mapX, mapZ), new THREE.MeshPhongMaterial( { map: groundTexture } ));
        
        groundMesh.position.x = (mapX / 2 ) - 0.5;
        groundMesh.position.z = (mapZ / 2) - 0.5;
        groundMesh.rotateX(- Math.PI / 2);
        groundMesh.updateMatrix();
        groundMesh.position.y = -0.51;
        groundMesh.name = "ground";
        groundMesh.receiveShadow = true;
        scene.add(groundMesh);

        
        // loadChunk(0,0);
        // loadChunk(1,1);
        // loadChunk(2,2);


        // indoorMidtownWalls.updateMatrix();
        // // worldGeometry.merge(indoorMidtownWalls.geometry, indoorMidtownWalls.matrix, 1);

        // indoorMidtownFloors.updateMatrix();
        // // worldGeometry.merge(indoorMidtownFloors.geometry, indoorMidtownFloors.matrix, 2);

      
        // var axesHelper = new THREE.AxesHelper( 5 );
        // scene.add( axesHelper );

        // var glassTexture = new  THREE.TextureLoader().load(Globals.TILES.ENTITIES.GLASS.TEXTURE);
        // var glassMaterial = new THREE.MeshLambertMaterial({ map: glassTexture, transparent: true });
        // glassMaterial.opacity = 0.75;
        // var glassCube = new THREE.Mesh(new THREE.BoxGeometry(1,1,0.8), glassMaterial);
        // glassCube.position.x = 11;
        // glassCube.position.y = 1;
        // glassCube.position.z = 10;
        // scene.add(glassCube);

        // var xAxisCube = new THREE.Mesh(new THREE.BoxGeometry(1,1,1), new THREE.MeshPhongMaterial({ color: 0xff0000, wireframe: true }));
        // xAxisCube.position.x = 0;
        // xAxisCube.position.y = 0;
        // xAxisCube.position.z = 0;
        // scene.add(xAxisCube);
       

        // var scuba = new THREE.Mesh(new THREE.BoxGeometry(1,0.6,0.1), new THREE.MeshPhongMaterial( { map: new THREE.TextureLoader().load("textures/scuba.png"), side: THREE.DoubleSide } ));
        // scuba.castShadow = true;
        // scuba.receiveShadow = true;
        // scuba.position.x = 9;
        // scuba.position.z = 10;
        // scuba.position.y = 3;
        // scene.add(scuba);

        // var neon = new THREE.Mesh(new THREE.BoxGeometry(1,0.6,0.1), new THREE.MeshPhongMaterial( { map: new THREE.TextureLoader().load("textures/neon2.png"), side: THREE.DoubleSide } ));
        // neon.castShadow = true;
        // neon.receiveShadow = true;
        // neon.position.x = 9;
        // neon.position.z = 10;
        // neon.position.y = 3.7;
        // scene.add(neon);

        // loadDoors(doors);
        // loadElectrixBoxes(boxes);
    }

    function socketUpdateMatrix (data) {
        console.log(data);
    }

    var animateInterval = 1000 / 90;

    var animate = function () {

        setTimeout( function() {

            try {
                
            } catch (e) {

            }
            
            

            stats.begin();

            TWEEN.update(delta);

            var delta = clock.getDelta();


            if (mixer) mixer.update( delta );


            requestAnimationFrame( animate );
            

            physicsDelta = 100 * (parseFloat(parseFloat(100) / (parseFloat(1) / parseFloat(delta))))

            for (var i = 0; i < worldPlayers.length; i++) {

                var player = worldPlayers[i];

                if ( player.id == currentPlayer.id ) {


                    let currentChunk = [
                        Math.round(player.position.x / chunkSize),
                        Math.round(player.position.z / chunkSize)
                    ];


                    var shouldBeLoaded = []; // Чанки которые будут загружены
                    for (var offsetX = -renderDistance; offsetX <= renderDistance; offsetX ++) {
                        for (var offsetZ = -renderDistance; offsetZ <= renderDistance; offsetZ ++) {
                            if (currentChunk[0]+offsetX >= 0 && currentChunk[1]+offsetX >= 0) {
                                let chunkName = "x" + (currentChunk[0]+offsetX) + "z" + (currentChunk[1]+offsetZ);
                                shouldBeLoaded.push(chunkName);
                                if (!loadedChunks.includes(chunkName) ) {
                                    // loadChunk(currentChunk[0]+offsetX, currentChunk[1]+offsetZ);
                                }    
                            }
                        }
                    }

                    // for (var cI = 0; cI < loadedChunks.length; cI++) {
                    //     var chunkName = loadedChunks[cI];
                    //     var index = shouldBeLoaded.indexOf(chunkName);
                    //     if (index == -1) {
                    //         // unloadChunkByName(chunkName)
                    //     } 
                    // }

                    // //     // if (loadedChunk) {
                    // //         // console.log(loadedChunk);
                    // //         // var regex = loadedChunk.match("^x(-?[0-9]*)z(-?[0-9]*)");
                    // //         // console.log(regex);
                    // //         // let loadedX = regex[1];
                    // //         // let loadedZ = regex[2];

                               
                    // //     // }
                        

                    // // }


                    if (keyboard.pressed("W")) {
                        currentPlayer.velocity.x = 1;
                        player.velocity.x = 1;
                    }

                    if (keyboard.pressed("S")) {
                        currentPlayer.velocity.x = -1;
                        player.velocity.x = -1;
                    }

                    if (keyboard.pressed("D")) {
                        currentPlayer.velocity.z = 1;
                        player.velocity.z = 1;
                    }

                    if (keyboard.pressed("A")) {
                        currentPlayer.velocity.z = -1;
                        player.velocity.z = -1;
                    }

                    if (keyboard.pressed("space")) {
                        currentPlayer.velocity.y = 1;
                        player.velocity.y = 1;
                    }

                    if (keyboard.pressed("E")) {
                        currentPlayer.rotation = new THREE.Vector3(1,0,0);
                        player.rotation = new THREE.Vector3(1,0,0);
                    }

                    if (keyboard.pressed("Q")) {
                        currentPlayer.rotation = new THREE.Vector3(0,0,1);
                        player.rotation = new THREE.Vector3(0,0,1);
                    }

                    // console.log(controls.getObject().rotation);    

                    sendState(player);

                    if (keyboard.pressed("F")) {
                         activateObject(Math.round(worldPlayers[i].position.x), Math.round(worldPlayers[i].position.y), Math.round(worldPlayers[i].position.z));
                    }
                }

                var x = player.position.x;
                var y = player.position.y;
                var z = player.position.z;
                
                if (player.velocity.x > 0) {
                    x += (player.speed / physicsDelta);
                }
                if (player.velocity.x < 0) {
                    x -= (player.speed / physicsDelta);
                }

                if (player.velocity.z > 0) {
                    z += (player.speed / physicsDelta);
                }
                if (player.velocity.z < 0) {
                    z -= (player.speed / physicsDelta);
                }

                if (player.velocity.y > 0) {
                    y += (20 / physicsDelta);
                }
                if (player.velocity.y < 0) {
                    y -= (player.gravity / physicsDelta);
                }

                worldPlayers[i].position.x = x;
                worldPlayers[i].position.y = y;
                worldPlayers[i].position.z = z;

                var ob = scene.getObjectByName("Player" + player.id);
                
                if ( ob ) {

                   ob.position.x = worldPlayers[i].position.x;
                   ob.position.y = worldPlayers[i].position.y + 0.05;
                   ob.position.z = worldPlayers[i].position.z;



                   // ob.rotation.set(worldPlayers[i].rotation);

                   // if (player.rotationVelocity > 0)
                   //  ob.rotateY((Math.PI / physicsDelta) * player.speed);
                   // else if (player.rotationVelocity < 0)
                   //  ob.rotateY((- Math.PI / physicsDelta) * player.speed);

                    if ( player.id == currentPlayer.id ) {

                        // moveCamera(
                             // camera.position.x = worldPlayers[i].position.x;
                             // camera.position.y = worldPlayers[i].position.y + 1;
                             // camera.position.z = worldPlayers[i].position.z;


                             controls.getObject().position.x = worldPlayers[i].position.x;
                             controls.getObject().position.y = worldPlayers[i].position.y + 1.6;
                             controls.getObject().position.z = worldPlayers[i].position.z;

                             let rotation = controls.getObject().rotation.y;

                             ob.rotation.y = rotation;
                             worldPlayers[i].rotation = rotation;

                             // .set(controls.getObject().rotation.clone());



                         // );


                       // camera.lookAt(new THREE.Vector3(player.position.x, player.position.y + 1.5, player.position.z));
                       // moveCamera(
                       //      worldPlayers[i].position.x - 1,
                       //      worldPlayers[i].position.y + 1.6,
                       //      worldPlayers[i].position.z - 1
                       //  );
                       // rotateCamera(ob.rotation.y);
                    } else {

                        // tweenRotation(ob, worldPlayers[i].rotation);

                        // ob.rotation.y = THREE.Math.lerp(worldPlayers[i].rotation,ob.rotation.y,0.15);

                        ob.rotation.y = worldPlayers[i].rotation;

                    }

                }


            }


          

            renderer.render(scene, camera);
            stats.end();

        }, animateInterval );

    };

    animate();


    // Тик от сервера

    function socketServerTick (data) {
        
        document.getElementById("netstat").innerHTML = "Online: " + data.roomPlayers.length;

        // Список всех игроков в игре
        worldPlayers = data.roomPlayers;

        // Пройтись по списку онлайн игроков
        for (var i = 0; i < worldPlayers.length; i++) {

            var player = worldPlayers[i];

            // Найти объект игрока на сцене
            var ob = scene.getObjectByName("Player" + player.id);

            if (! ob ) {


                var objGeo = new THREE.Geometry();
                objGeo.merge(new THREE.BoxGeometry(player.xlen, player.ylen, player.zlen));

                
                var obj = new THREE.Mesh(
                    objGeo,
                    new THREE.MeshPhongMaterial({ color: 0xff0000, wireframe: true })
                );

                var geometry = new THREE.Geometry();
                geometry.vertices.push(
                    new THREE.Vector3( 0, 0, -1 ),
                    new THREE.Vector3( 0, 0, 0 )
                );

                var line = new THREE.Line( geometry, new THREE.LineBasicMaterial({ color: 0x0000ff }) );
                // scene.add(line);
                obj.add(line);

                obj.userData.name = player.id;
                obj.name = "Player" + player.id;
                obj.position.set( player.position.x, player.position.y, player.position.z);
                // obj.rotation.set( player.rotation )

                // obj.updateMatrix();
                scene.add(obj);

            }

        }

    }

    function sendState(player) {
        socket.emit("clientSendState", {
            "id"       : player.id,
            "velocity" : player.velocity,
            "rotation" : player.rotation
        });
        currentPlayer.velocity.x = currentPlayer.velocity.y = currentPlayer.velocity.z = 0;
    }

    function activateObject(x, y, z){
        socket.emit("clientActivateObject", {
            "id"       : currentPlayer.id,
            "obj"      : {
                x: x,
                y: y,
                z: z
            }
        });
    }

}


// var scale = 1;
// var mouseX = 0;
// var mouseY = 0;

// camera.rotation.order = "XYZ"; // this is not the default

// document.addEventListener( "mousemove", mouseMove, false );

// function mouseMove( event ) {

//     mouseX = - ( event.clientX / renderer.domElement.clientWidth ) * 2 + 1;
//     mouseY = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;

//     camera.rotation.x = mouseY / scale;
//     camera.rotation.y = mouseX / scale;

// }
